{"frames": {

"barra_2x.png":
{
	"frame": {"x":1350,"y":531,"w":57,"h":106},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":57,"h":106},
	"sourceSize": {"w":57,"h":106}
},
"boton.png":
{
	"frame": {"x":2,"y":1428,"w":225,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":88},
	"sourceSize": {"w":225,"h":88}
},
"boton_2x.png":
{
	"frame": {"x":1350,"y":441,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"btn_ayuda_2x.png":
{
	"frame": {"x":1374,"y":274,"w":109,"h":109},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":109,"h":109},
	"sourceSize": {"w":109,"h":109}
},
"btn_dificil_2x.png":
{
	"frame": {"x":731,"y":1245,"w":285,"h":105},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":285,"h":105},
	"sourceSize": {"w":285,"h":105}
},
"btn_facil_2x.png":
{
	"frame": {"x":1018,"y":1245,"w":285,"h":104},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":285,"h":104},
	"sourceSize": {"w":285,"h":104}
},
"btn_iniciar_2x.png":
{
	"frame": {"x":2,"y":1245,"w":452,"h":181},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":452,"h":181},
	"sourceSize": {"w":452,"h":181}
},
"btn_iniciar_Facebook2x.png":
{
	"frame": {"x":1024,"y":681,"w":242,"h":74},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":242,"h":74},
	"sourceSize": {"w":242,"h":74}
},
"btn_medio_2x.png":
{
	"frame": {"x":731,"y":1352,"w":285,"h":104},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":285,"h":104},
	"sourceSize": {"w":285,"h":104}
},
"circulo.png":
{
	"frame": {"x":620,"y":1425,"w":57,"h":46},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":57,"h":46},
	"sourceSize": {"w":57,"h":46}
},
"cuerdas_dificil_2x.png":
{
	"frame": {"x":229,"y":1428,"w":146,"h":54},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":146,"h":54},
	"sourceSize": {"w":146,"h":54}
},
"cuerdas_facil_2x.png":
{
	"frame": {"x":1288,"y":1111,"w":145,"h":121},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":145,"h":121},
	"sourceSize": {"w":145,"h":121}
},
"cuerdas_iniciar_2x.png":
{
	"frame": {"x":1018,"y":1351,"w":226,"h":123},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":226,"h":123},
	"sourceSize": {"w":226,"h":123}
},
"cuerdas_medio_2x.png":
{
	"frame": {"x":1350,"y":385,"w":146,"h":54},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":146,"h":54},
	"sourceSize": {"w":146,"h":54}
},
"estrella_grande.png":
{
	"frame": {"x":377,"y":1428,"w":83,"h":79},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":83,"h":79},
	"sourceSize": {"w":83,"h":79}
},
"estrella_pequena.png":
{
	"frame": {"x":548,"y":1425,"w":70,"h":69},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":70,"h":69},
	"sourceSize": {"w":70,"h":69}
},
"fondo.png":
{
	"frame": {"x":1288,"y":681,"w":240,"h":243},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":240,"h":243},
	"sourceSize": {"w":240,"h":243}
},
"fondo_2x.png":
{
	"frame": {"x":1024,"y":358,"w":324,"h":321},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":324,"h":321},
	"sourceSize": {"w":324,"h":321}
},
"fondo_Juego_2x.png":
{
	"frame": {"x":1024,"y":2,"w":348,"h":354},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":348,"h":354},
	"sourceSize": {"w":348,"h":354}
},
"fondo_marcos.png":
{
	"frame": {"x":1288,"y":926,"w":188,"h":183},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":188,"h":183},
	"sourceSize": {"w":188,"h":183}
},
"fondo_nivel.png":
{
	"frame": {"x":462,"y":1425,"w":84,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":84,"h":64},
	"sourceSize": {"w":84,"h":64}
},
"fondo_texto.png":
{
	"frame": {"x":229,"y":1484,"w":44,"h":29},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":44,"h":29},
	"sourceSize": {"w":44,"h":29}
},
"g.png":
{
	"frame": {"x":1478,"y":926,"w":43,"h":59},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":7,"y":0,"w":43,"h":59},
	"sourceSize": {"w":57,"h":59}
},
"logo_2x.png":
{
	"frame": {"x":456,"y":1245,"w":273,"h":178},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":273,"h":178},
	"sourceSize": {"w":273,"h":178}
},
"marco_con_fondo.png":
{
	"frame": {"x":1374,"y":2,"w":137,"h":134},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":137,"h":134},
	"sourceSize": {"w":137,"h":134}
},
"marco_sin_fondo.png":
{
	"frame": {"x":1374,"y":138,"w":137,"h":134},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":137,"h":134},
	"sourceSize": {"w":137,"h":134}
},
"progreso_barra_2x.png":
{
	"frame": {"x":1485,"y":274,"w":43,"h":81},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":43,"h":81},
	"sourceSize": {"w":43,"h":81}
},
"reloj.png":
{
	"frame": {"x":1305,"y":1234,"w":125,"h":125},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":125,"h":125},
	"sourceSize": {"w":125,"h":125}
},
"reloj_2x.png":
{
	"frame": {"x":1440,"y":441,"w":81,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":81,"h":80},
	"sourceSize": {"w":81,"h":80}
},
"score_2x.png":
{
	"frame": {"x":1246,"y":1361,"w":156,"h":104},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":156,"h":104},
	"sourceSize": {"w":156,"h":104}
},
"transparencia_2x.png":
{
	"frame": {"x":2,"y":2,"w":1020,"h":765},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":1020,"h":765},
	"sourceSize": {"w":1020,"h":765}
},
"tuto1_2x.png":
{
	"frame": {"x":2,"y":769,"w":641,"h":474},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":641,"h":474},
	"sourceSize": {"w":641,"h":474}
},
"tuto2_2x.png":
{
	"frame": {"x":645,"y":769,"w":641,"h":474},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":641,"h":474},
	"sourceSize": {"w":641,"h":474}
},
"v.png":
{
	"frame": {"x":1350,"y":639,"w":43,"h":39},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":43,"h":39},
	"sourceSize": {"w":43,"h":39}
},
"y.png":
{
	"frame": {"x":1435,"y":1111,"w":29,"h":42},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":7,"y":0,"w":29,"h":42},
	"sourceSize": {"w":43,"h":42}
}},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker",
	"version": "1.0",
	"image": "AtlasMiniJuego2x.png",
	"format": "RGBA8888",
	"size": {"w":1530,"h":1530},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:a87267777a3677a4354971bb81d02489:84133baf9fd81cdf27b47c6a885e8a94:709de817d475c595793ece697dd7a6ed$"
}
}
