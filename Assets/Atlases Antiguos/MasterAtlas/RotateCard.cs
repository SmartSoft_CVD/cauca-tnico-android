﻿using UnityEngine;
using System.Collections;

public class RotateCard : MonoBehaviour {

	public bool voltear;
	public bool apagar;
	public TweenRotation tween;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.D))
		{
			voltear = !voltear;
		}

		if(Input.GetKeyDown(KeyCode.S))
		{
			apagar = !apagar;
		}
	
		if(voltear)
			tween.PlayForward();
		else
			tween.PlayReverse();

		if(apagar)
			this.gameObject.GetComponent<UIButton>().isEnabled = false;
		else
			this.gameObject.GetComponent<UIButton>().isEnabled = true;
	}

	public void VoltearCarta()
	{
		voltear = !voltear;
	}
}
