﻿using UnityEngine;
using System.Collections;

public class SelectionItemsRegion : MonoBehaviour
{
	private string regionCurrent;
	public UIGrid [] gridItemsRegion;
	public GameObject gridItemsCurrent;

	private GameObject gridCurrent;
	public GameObject parentGrids;

	public UIScrollBar scrollBar;
	private UIScrollView scrollView;

	public UISprite [] spritesColor;
	public Color [] coloresSprites;
	public UIWidget [] mWidget;

	public UIWidget mWidget1;



	public int indiceColor;
	void Awake()
	{
		regionCurrent = PlayerPrefs.GetString("RegionActual");

		scrollView = parentGrids.GetComponent<UIScrollView>();

		mWidget = new UIWidget[3];

		mWidget[0] = spritesColor[0].GetComponent<UIWidget>();
		mWidget[1] = spritesColor[1].GetComponent<UIWidget>();
		mWidget[2] = spritesColor[2].GetComponent<UIWidget>();

		switch (regionCurrent)
		{
		case "Campesinos":
			
			scrollView.ResetPosition();
			//Caras
			gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCaras"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			gridItemsCurrent.name = "Caras";			
			// Lo agregamos como hijo del HUD
			gridItemsCurrent.transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
			gridItemsCurrent.transform.localRotation = Quaternion.identity;
			gridItemsCurrent.transform.localScale = Vector3.one;
			
			//			gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
			//			gridItemsCurrent.GetComponent<UIGrid>().Reposition();
			scrollView.ResetPosition();
			scrollBar.ForceUpdate();
			
			CambiarColor(0);
			indiceColor = 0;
			PlayerPrefs.SetInt("ColorAvatar", 0);
			
			
			break;
		case "Afro":
			
			scrollView.ResetPosition();
			//Caras
			gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCaras"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			gridItemsCurrent.name = "Caras";			
			// Lo agregamos como hijo del HUD
			gridItemsCurrent.transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
			gridItemsCurrent.transform.localRotation = Quaternion.identity;
			gridItemsCurrent.transform.localScale = Vector3.one;
			
			scrollView.ResetPosition();
			scrollBar.ForceUpdate();
			
			CambiarColor(1);
			indiceColor = 1;
			
			PlayerPrefs.SetInt("ColorAvatar", 1);
			//			gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
			//			gridItemsCurrent.GetComponent<UIGrid>().Reposition();
			break;
		case "Guambianos":
			
			scrollView.ResetPosition();
			//Caras
			gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCaras"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			gridItemsCurrent.name = "Caras";			
			// Lo agregamos como hijo del HUD
			gridItemsCurrent.transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
			gridItemsCurrent.transform.localRotation = Quaternion.identity;
			gridItemsCurrent.transform.localScale = Vector3.one;
			
			scrollView.ResetPosition();
			scrollBar.ForceUpdate();
			//			gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
			//			gridItemsCurrent.GetComponent<UIGrid>().Reposition();
			
			CambiarColor(2);
			indiceColor = 2;
			
			PlayerPrefs.SetInt("ColorAvatar", 2);
			break;
		case "Nasa":
			
			scrollView.ResetPosition();
			//Caras
			gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCaras"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			gridItemsCurrent.name = "Caras";			
			// Lo agregamos como hijo del HUD
			gridItemsCurrent.transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
			gridItemsCurrent.transform.localRotation = Quaternion.identity;
			gridItemsCurrent.transform.localScale = Vector3.one;
			
			scrollView.ResetPosition();
			//			gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
			//			gridItemsCurrent.GetComponent<UIGrid>().Reposition();
			scrollBar.ForceUpdate();
			
			CambiarColor(3);
			indiceColor = 3;
			
			PlayerPrefs.SetInt("ColorAvatar", 3);
			break;
		case "Yanaconas":
			
			scrollView.ResetPosition();
			//Caras
			gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCaras"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			gridItemsCurrent.name = "Caras";			
			// Lo agregamos como hijo del HUD
			gridItemsCurrent.transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
			gridItemsCurrent.transform.localRotation = Quaternion.identity;
			gridItemsCurrent.transform.localScale = Vector3.one;
			
			scrollView.ResetPosition();
			scrollBar.ForceUpdate();
			
			CambiarColor(4);
			indiceColor = 4;
			
			PlayerPrefs.SetInt("ColorAvatar", 4);
			//			gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
			//			gridItemsCurrent.GetComponent<UIGrid>().Reposition();
			break;
		default:
			break;
		}


	}

	void CambiarColor(int indiceColor)
	{

	}

	void Update()
	{
//		foreach (UIWidget spritecolor in mWidget)
//		{
//			spritecolor.color = coloresSprites[indiceColor];
//		}

//		if(mWidget[0] != null)
//			mWidget[0].color = coloresSprites[indiceColor];
//		if(mWidget[1] != null)
//			mWidget[1].color = coloresSprites[indiceColor];
//		if(mWidget[2] != null)
//			mWidget[2].color = coloresSprites[indiceColor];

		scrollView.UpdatePosition();
	}
	// Use this for initialization
	void Start ()
	{


//		switch (regionCurrent)
//		{
//		case "Campesinos":
//
//			scrollView.ResetPosition();
//			//Caras
//			gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCaras"));			
//			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
//			gridItemsCurrent.name = "Caras";			
//			// Lo agregamos como hijo del HUD
//			gridItemsCurrent.transform.parent = parentGrids.transform;		
//			// La asignaciom modifica el Transform, asi que lo reiniciamos
//			gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
//			gridItemsCurrent.transform.localRotation = Quaternion.identity;
//			gridItemsCurrent.transform.localScale = Vector3.one;
//
////			gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
////			gridItemsCurrent.GetComponent<UIGrid>().Reposition();
//			scrollView.ResetPosition();
//			scrollBar.ForceUpdate();
//
//			CambiarColor(0);
//			indiceColor = 0;
//			PlayerPrefs.SetInt("ColorAvatar", 0);
//			
//			
//			break;
//		case "Afro":
//
//			scrollView.ResetPosition();
//			//Caras
//			gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCaras"));			
//			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
//			gridItemsCurrent.name = "Caras";			
//			// Lo agregamos como hijo del HUD
//			gridItemsCurrent.transform.parent = parentGrids.transform;		
//			// La asignaciom modifica el Transform, asi que lo reiniciamos
//			gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
//			gridItemsCurrent.transform.localRotation = Quaternion.identity;
//			gridItemsCurrent.transform.localScale = Vector3.one;
//
//			scrollView.ResetPosition();
//			scrollBar.ForceUpdate();
//
//			CambiarColor(1);
//			indiceColor = 1;
//
//			PlayerPrefs.SetInt("ColorAvatar", 1);
////			gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
////			gridItemsCurrent.GetComponent<UIGrid>().Reposition();
//			break;
//		case "Guambianos":
//
//			scrollView.ResetPosition();
//			//Caras
//			gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCaras"));			
//			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
//			gridItemsCurrent.name = "Caras";			
//			// Lo agregamos como hijo del HUD
//			gridItemsCurrent.transform.parent = parentGrids.transform;		
//			// La asignaciom modifica el Transform, asi que lo reiniciamos
//			gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
//			gridItemsCurrent.transform.localRotation = Quaternion.identity;
//			gridItemsCurrent.transform.localScale = Vector3.one;
//
//			scrollView.ResetPosition();
//			scrollBar.ForceUpdate();
////			gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
////			gridItemsCurrent.GetComponent<UIGrid>().Reposition();
//
//			CambiarColor(2);
//			indiceColor = 2;
//
//			PlayerPrefs.SetInt("ColorAvatar", 2);
//			break;
//		case "Nasa":
//
//			scrollView.ResetPosition();
//			//Caras
//			gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCaras"));			
//			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
//			gridItemsCurrent.name = "Caras";			
//			// Lo agregamos como hijo del HUD
//			gridItemsCurrent.transform.parent = parentGrids.transform;		
//			// La asignaciom modifica el Transform, asi que lo reiniciamos
//			gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
//			gridItemsCurrent.transform.localRotation = Quaternion.identity;
//			gridItemsCurrent.transform.localScale = Vector3.one;
//
//			scrollView.ResetPosition();
////			gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
////			gridItemsCurrent.GetComponent<UIGrid>().Reposition();
//			scrollBar.ForceUpdate();
//
//			CambiarColor(3);
//			indiceColor = 3;
//
//			PlayerPrefs.SetInt("ColorAvatar", 3);
//			break;
//		case "Yanaconas":
//
//			scrollView.ResetPosition();
//			//Caras
//			gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCaras"));			
//			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
//			gridItemsCurrent.name = "Caras";			
//			// Lo agregamos como hijo del HUD
//			gridItemsCurrent.transform.parent = parentGrids.transform;		
//			// La asignaciom modifica el Transform, asi que lo reiniciamos
//			gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
//			gridItemsCurrent.transform.localRotation = Quaternion.identity;
//			gridItemsCurrent.transform.localScale = Vector3.one;
//
//			scrollView.ResetPosition();
//			scrollBar.ForceUpdate();
//
//			CambiarColor(4);
//			indiceColor = 4;
//
//			PlayerPrefs.SetInt("ColorAvatar", 4);
////			gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
////			gridItemsCurrent.GetComponent<UIGrid>().Reposition();
//			break;
//		default:
//			break;
//		}
	
	}
	


	public void SelectItemsRegion()
	{
		if(UIToggle.current.value && UIToggle.current.group == 7 && UIToggle.current.name == "Toggle")
		{
			PlayerPrefs.SetString("textoActual", "");
			if (gridItemsCurrent != null) {
				
				// Desactivamos la pantalla actual
				gridItemsCurrent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (gridItemsCurrent);
				gridItemsCurrent = null;
			}

			scrollView.ResetPosition();

			switch (regionCurrent)
			{
				case "Campesinos":

				//Caras
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCaras"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Caras";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;


//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
					break;

				case "Afro":

				//Caras
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCaras"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Caras";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
					break;

				case "Guambianos":

				//Caras
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCaras"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Caras";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
					break;

				case "Nasa":

				//Caras
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCaras"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Caras";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
					break;

				case "Yanaconas":

				//Caras
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCaras"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Caras";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
					break;
			default:
				break;
			}

			scrollView.ResetPosition();
			scrollBar.ForceUpdate();
		}


		if(UIToggle.current.value && UIToggle.current.group == 7 && UIToggle.current.name == "Toggle2")
		{
			PlayerPrefs.SetString("textoActual", "");
			scrollView.ResetPosition();
			if (gridItemsCurrent != null) {
				
				// Desactivamos la pantalla actual
				gridItemsCurrent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (gridItemsCurrent);
				gridItemsCurrent = null;
			}

			switch (regionCurrent)
			{
			case "Campesinos":
				
				//Caras
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCabellos"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Cabellos";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				break;
				
			case "Afro":
				
				//Caras
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCabellos"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Cabellos";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				break;
				
			case "Guambianos":
				
				//Caras
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCabellos"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Cabellos";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				break;
				
			case "Nasa":
				
				//Caras
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCabellos"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Cabellos";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				break;
				
			case "Yanaconas":
				
				//Caras
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCabellos"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Cabellos";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				break;
			default:
				break;
			}

			scrollView.ResetPosition();

			scrollBar.ForceUpdate();
		}

		// Cambiar Camisas
		if(UIToggle.current.value && UIToggle.current.group == 7 && UIToggle.current.name == "Toggle3")
		{
			PlayerPrefs.SetString("textoActual", "");
			if (gridItemsCurrent != null) {
				
				// Desactivamos la pantalla actual
				gridItemsCurrent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (gridItemsCurrent);
				gridItemsCurrent = null;
			}

			scrollView.ResetPosition();

			switch (regionCurrent)
			{
			case "Campesinos":

				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCamisasCampesinos"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Camisas Campesinos";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();

				break;
				
			case "Afro":

				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCamisasAfro"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Camisas Afro";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();

				break;
				
			case "Guambianos":

				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCamisasGuambianos"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Camisas Guambianos";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				break;
				
			case "Nasa":

				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCamisasNasa"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Camisas Nasa";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				break;
				
			case "Yanaconas":


				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridCamisasYanaconas"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Camisas Yanaconas";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;


//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				break;
			default:
				break;
			}

			scrollView.ResetPosition();

			scrollBar.ForceUpdate();
		}

		// --------------------------------------------------------------------------------------------------------------------------- //

		// Cambiar Pantalones
		if(UIToggle.current.value && UIToggle.current.group == 7 && UIToggle.current.name == "Toggle4")
		{
			PlayerPrefs.SetString("textoActual", "");
			if (gridItemsCurrent != null) {
				
				// Desactivamos la pantalla actual
				gridItemsCurrent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (gridItemsCurrent);
				gridItemsCurrent = null;
			}

			scrollView.ResetPosition();

			switch (regionCurrent)
			{
			case "Campesinos":
				
				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridPantalonesCampesinos"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Pantalones Campesinos";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				
				break;
				
			case "Afro":
				
				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridPantalonesAfros"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Pantalones Afro";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				
				break;
				
			case "Guambianos":
				
				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridPantalonesGuambianos"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Pantalones Guambianos";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				
				break;
				
			case "Nasa":
				
				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridPantalonesNasa"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Pantalones Nasa";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				
				
				break;
				
			case "Yanaconas":
				
				
				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridPantalonesYanaconas"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Pantalones Yanaconas";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				
				break;
			default:
				break;
			}

			scrollView.ResetPosition();

			scrollBar.ForceUpdate();
		}

		// --------------------------------------------------------------------------------------------------------------------------- //
		
		// Cambiar Zapatos
		if(UIToggle.current.value && UIToggle.current.group == 7 && UIToggle.current.name == "Toggle5")
		{
			PlayerPrefs.SetString("textoActual", "");
			if (gridItemsCurrent != null) {
				
				// Desactivamos la pantalla actual
				gridItemsCurrent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (gridItemsCurrent);
				gridItemsCurrent = null;
			}

			scrollView.ResetPosition();

			switch (regionCurrent)
			{
			case "Campesinos":
				
				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridZapatosCampesinos"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Zapatos Campesinos";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				break;
				
			case "Afro":
				
				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridZapatosAfro"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Zapatos Afro";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				
				break;
				
			case "Guambianos":
				
				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridZapatosGuambianos"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Zapatos Guambianos";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				
				break;
				
			case "Nasa":
				
				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridZapatosNasa"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Zapatos Nasa";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				
				
				break;
				
			case "Yanaconas":
				
				
				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridZapatosYanaconas"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Zapatos Yanaconas";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				break;
			default:
				break;
			}

			scrollView.ResetPosition();

			scrollBar.ForceUpdate();
		}

		// --------------------------------------------------------------------------------------------------------------------------- //
		
		// Cambiar Accesorios
		if(UIToggle.current.value && UIToggle.current.group == 7 && UIToggle.current.name == "Toggle6")
		{
			PlayerPrefs.SetString("textoActual", "");
			if (gridItemsCurrent != null) {
				
				// Desactivamos la pantalla actual
				gridItemsCurrent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (gridItemsCurrent);
				gridItemsCurrent = null;
			}

			scrollView.ResetPosition();

			switch (regionCurrent)
			{
			case "Campesinos":
				
				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridAccesoriosCampesinos"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Accesorios Campesinos";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				
				break;
				
			case "Afro":
				
				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridAccesoriosAfros"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Accesorios Afro";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;


//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				break;
				
			case "Guambianos":
				
				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridAccesoriosGuambianos"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Accesorios Guambianos";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				
				break;
				
			case "Nasa":
				
				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridAccesoriosNasa"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Accesorios Nasa";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				
				
				break;
				
			case "Yanaconas":
				
				
				//Camisas
				gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Grids/GridAccesoriosYanaconas"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				gridItemsCurrent.name = "Accesorios Yanaconas";			
				// Lo agregamos como hijo del HUD
				gridItemsCurrent.transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				gridItemsCurrent.transform.localPosition = new Vector3(-40, 211, 0);
				gridItemsCurrent.transform.localRotation = Quaternion.identity;
				gridItemsCurrent.transform.localScale = Vector3.one;

//				gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
//				gridItemsCurrent.GetComponent<UIGrid>().Reposition();
				
				break;
			default:
				break;
			}

			scrollView.ResetPosition();

			scrollBar.ForceUpdate();
		}
	}
}
