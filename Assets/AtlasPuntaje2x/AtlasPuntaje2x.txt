{"frames": {

"boton.png":
{
	"frame": {"x":192,"y":274,"w":225,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":88},
	"sourceSize": {"w":225,"h":88}
},
"estrella_grande.png":
{
	"frame": {"x":192,"y":364,"w":83,"h":79},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":83,"h":79},
	"sourceSize": {"w":83,"h":79}
},
"estrella_pequena.png":
{
	"frame": {"x":363,"y":364,"w":70,"h":69},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":70,"h":69},
	"sourceSize": {"w":70,"h":69}
},
"fondo.png":
{
	"frame": {"x":2,"y":2,"w":240,"h":243},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":240,"h":243},
	"sourceSize": {"w":240,"h":243}
},
"fondo_marcos.png":
{
	"frame": {"x":2,"y":247,"w":188,"h":183},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":188,"h":183},
	"sourceSize": {"w":188,"h":183}
},
"fondo_nivel.png":
{
	"frame": {"x":277,"y":364,"w":84,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":84,"h":64},
	"sourceSize": {"w":84,"h":64}
},
"fondo_texto.png":
{
	"frame": {"x":383,"y":2,"w":44,"h":29},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":44,"h":29},
	"sourceSize": {"w":44,"h":29}
},
"marco_con_fondo.png":
{
	"frame": {"x":244,"y":2,"w":137,"h":134},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":137,"h":134},
	"sourceSize": {"w":137,"h":134}
},
"marco_sin_fondo.png":
{
	"frame": {"x":244,"y":138,"w":137,"h":134},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":137,"h":134},
	"sourceSize": {"w":137,"h":134}
}},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker",
	"version": "1.0",
	"image": "AtlasPuntaje2x.png",
	"format": "RGBA8888",
	"size": {"w":445,"h":445},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:e9be02179fe0078f4d4d05c557a964e2:62eb2ff2d1d2a516fe825a8be5a7865b:0af993ac086f9c8f232063bf9c7cd8eb$"
}
}
