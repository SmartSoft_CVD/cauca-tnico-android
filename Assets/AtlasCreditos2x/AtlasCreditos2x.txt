{"frames": {

"btn_menu.png":
{
	"frame": {"x":932,"y":222,"w":83,"h":83},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":83,"h":83},
	"sourceSize": {"w":83,"h":83}
},
"btn_menu_pausa.png":
{
	"frame": {"x":2,"y":833,"w":116,"h":116},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":116,"h":116},
	"sourceSize": {"w":116,"h":116}
},
"btn_pausa.png":
{
	"frame": {"x":238,"y":833,"w":109,"h":109},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":109,"h":109},
	"sourceSize": {"w":109,"h":109}
},
"btn_play.png":
{
	"frame": {"x":853,"y":307,"w":138,"h":138},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":138,"h":138},
	"sourceSize": {"w":138,"h":138}
},
"btn_reiniciar.png":
{
	"frame": {"x":120,"y":833,"w":116,"h":116},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":116,"h":116},
	"sourceSize": {"w":116,"h":116}
},
"btn_tutorial.png":
{
	"frame": {"x":933,"y":131,"w":89,"h":89},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":89,"h":89},
	"sourceSize": {"w":89,"h":89}
},
"creditos_2x.png":
{
	"frame": {"x":646,"y":401,"w":120,"h":391},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":120,"h":391},
	"sourceSize": {"w":120,"h":391}
},
"globo.png":
{
	"frame": {"x":646,"y":2,"w":328,"h":127},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":328,"h":127},
	"sourceSize": {"w":328,"h":127}
},
"logo.png":
{
	"frame": {"x":2,"y":478,"w":538,"h":353},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":538,"h":353},
	"sourceSize": {"w":538,"h":353}
},
"opacidad.png":
{
	"frame": {"x":646,"y":239,"w":205,"h":160},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":205,"h":160},
	"sourceSize": {"w":205,"h":160}
},
"texto_ojo.png":
{
	"frame": {"x":646,"y":175,"w":284,"h":62},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":284,"h":62},
	"sourceSize": {"w":284,"h":62}
},
"texto_reloj.png":
{
	"frame": {"x":646,"y":131,"w":285,"h":42},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":285,"h":42},
	"sourceSize": {"w":285,"h":42}
},
"tutorial_new.png":
{
	"frame": {"x":2,"y":2,"w":642,"h":474},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":642,"h":474},
	"sourceSize": {"w":642,"h":474}
}},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker",
	"version": "1.0",
	"image": "AtlasCreditos2x.png",
	"format": "RGBA8888",
	"size": {"w":1024,"h":1024},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:14fd148f278e306a89d2e4035d201cd9:1de849f44cafd59aade8d24870e57364:70257d93f47692da7c2bc8ce068eece3$"
}
}
