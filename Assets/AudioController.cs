﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour {

	public AudioClip [] clipsLoop;


	void PlaySoundRandom()
	{
		if(audio.isPlaying) return;
		audio.clip = clipsLoop[Random.Range(0, clipsLoop.Length)];
		audio.Play();
	}
	// Use this for initialization
	void Start ()
	{
		PlaySoundRandom();
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(PlayerPrefsX.GetBool("Mute"))
		{
			this.gameObject.GetComponent<AudioSource>().volume = 0;
		}
		else
		{
			this.gameObject.GetComponent<AudioSource>().volume = 1;
		}
	}

}
