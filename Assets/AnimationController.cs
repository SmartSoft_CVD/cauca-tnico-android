﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour {

	public TweenPosition [] tweens;
	public int numberGroup;
	public bool inOrOut;

	// Use this for initialization
	void Start ()
	{
		foreach (TweenPosition currentTween in tweens)
		{
			if(currentTween.tweenGroup == numberGroup)
			{
				if(inOrOut)
					currentTween.method = UITweener.Method.BounceIn;
				else
					currentTween.method = UITweener.Method.BounceOut;
			}
				
		}


	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
