﻿using UnityEngine;
using System.Collections;

public class MenuFromChoose : MonoBehaviour
{
	public GameObject [] buttons;

	// Use this for initialization
	void Start ()
	{
		if(PlayerPrefsX.GetBool("Escoge"))
		{
			NGUITools.SetActive(buttons[0], false);
			NGUITools.SetActive(buttons[1], false);
			NGUITools.SetActive(buttons[2], true);
		}
		else
		{
			NGUITools.SetActive(buttons[0], true);
			NGUITools.SetActive(buttons[1], true);
			NGUITools.SetActive(buttons[2], false);
		}

		PlayerPrefsX.SetBool("Escoge", false);
	}
}
