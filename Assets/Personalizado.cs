﻿using UnityEngine;
using System.Collections;

public class Personalizado : MonoBehaviour {

	public UISprite [] partesPersonalizadas;
	public string [] partes;

	// Use this for initialization
	void Start ()
	{
		partes = new string[5];
		partes = TabsController.ObtenerCaracteristicas(partes);

		for (int i = 0; i < partesPersonalizadas.Length; i++)
		{
			partesPersonalizadas[i].spriteName = partes[i];
		}

	}
}
