﻿using UnityEngine;
using System.Collections;

public class PartsReceived : MonoBehaviour
{
	public UISprite [] partsToReceive;
	private string[]  keys;

	public UILabel textoActual;

	public bool actualizarTexto;
	public UIButton botonSiguiente;

	public bool estaEnCustomizar = false;

	public static bool [] restricciones;
	public bool pasar = false;

	public GameObject confettiPrefab;
	public GameObject parentConfetti;

	public static bool unaVez = false;


	ParticleEmitter emisor;

	public GameObject conffeti;
	GameObject con;

	public GameObject parte1;
	public GameObject parte2;
	public GameObject parte3;
	public GameObject parte4;
	public GameObject parte5;

	// Use this for initialization
	void Start ()
	{
		partsToReceive[1].enabled = false;
		partsToReceive[3].enabled = false;
		partsToReceive[4].enabled = false;

		PlayerPrefsX.SetBool("Confetti", false);

		Debug.LogError("Pref: " + PlayerPrefsX.GetBool("Confetti").ToString());
		if (conffeti != null) {
			
			// Desactivamos la pantalla actual
			conffeti.SetActive (false); // (opcional)
			
			// Eliminamos HUD padre
			GameObject.Destroy (conffeti);
			conffeti = null;
		}

		if(estaEnCustomizar)
		{
			botonSiguiente.isEnabled = false;
		}

		keys = new string[7];

		keys[0] = "Cara";
		keys[1] = "Cabeza";
		keys[2] = "Camisa";
		keys[3] = "Pantalon";
		keys[4] = "Zapatos";
		keys[5] = "Accesorios";
		keys[6] = "Sombreros";


		restricciones = new bool[7];

		for (int f = 0; f < restricciones.Length; f++)
		{
			restricciones[f] = false;
		}



		if(estaEnCustomizar)
		{
			EventDelegate.Add(botonSiguiente.onClick, delegate()
			                  {


				if(pasar)
				{
					NavigationMap.ChangeOfScreen(Hud.hudAV_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.GIVE_PERSONALITY);
				}
					

				
				});


		}
	

	}

	void InstanciarConfetti(float timeLife)
	{
	

		if(PlayerPrefsX.GetBool("Confetti") == false)
		{
			if(con == null)
			{
				con = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Confetti"));

				PlayerPrefs.GetInt("TimesParticles", 0);

				int timesParticles = 0;

				con.name = "Conffeti_" + PlayerPrefs.GetInt("TimesParticles", 0);

				timesParticles++;

				PlayerPrefs.SetInt("TimesParticles", timesParticles);
				con.transform.parent = parentConfetti.transform;
				
				//			conffeti = NGUITools.AddChild(parentConfetti, confettiPrefab);
				con.transform.localPosition = new Vector3(0, 1000f, 0);
				con.transform.localRotation = new Quaternion(0, -180f, 0, 0);
				con.transform.localScale = Vector3.one;



				//conffeti = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Confetti"));

	//			conffeti.name = "Conffeti";
	//			conffeti.transform.parent = parentConfetti.transform;
	//
	////			conffeti = NGUITools.AddChild(parentConfetti, confettiPrefab);
	//			conffeti.transform.localPosition = new Vector3(0, 1000f, 0);
	//			conffeti.transform.localRotation = new Quaternion(0, -180f, 0, 0);
	//			conffeti.transform.localScale = Vector3.one;

				PlayerPrefsX.SetBool("Confetti", true);
			}





			if(con != null)
			{

				Destroy(con, timeLife);
				con = null;


			}

		}
	}

	void CargarParticles()
	{
		#region
		if(parte1 == null)
		{
			parte1 = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Particles/Violet"));		
			parte1.name = "Violet";
			parte1.transform.parent = this.transform;		
			parte1.transform.localPosition = Vector3.zero;
			parte1.transform.localRotation = new Quaternion(90.0f, 180f, 0, 0);
			parte1.transform.localScale = Vector3.one;
		}
		#endregion
		
		#region
		if(parte2 == null)
		{
			parte2 = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Particles/Yellow"));		
			parte2.name = "Yellow";
			parte2.transform.parent = this.transform;		
			parte2.transform.localPosition = Vector3.zero;
			parte2.transform.localRotation = new Quaternion(90.0f, 180f, 0, 0);
			parte2.transform.localScale = Vector3.one;
		}
		#endregion
		
		#region
		if(parte3 == null)
		{
			parte3 = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Particles/Green"));		
			parte3.name = "Green";
			parte3.transform.parent = this.transform;		
			parte3.transform.localPosition = Vector3.zero;
			parte3.transform.localRotation = new Quaternion(90.0f, 180f, 0, 0);
			parte3.transform.localScale = Vector3.one;
		}
		#endregion
		
		#region
		if(parte4 == null)
		{
			parte4 = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Particles/Blue"));		
			parte4.name = "Blue";
			parte4.transform.parent = this.transform;		
			parte4.transform.localPosition = Vector3.zero;
			parte4.transform.localRotation = new Quaternion(90.0f, 180f, 0, 0);
			parte4.transform.localScale = Vector3.one;
		}
		#endregion
		
		#region
		if(parte5 == null)
		{
			parte5 = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Particles/Red"));		
			parte5.name = "Red";
			parte5.transform.parent = this.transform;		
			parte5.transform.localPosition = Vector3.zero;
			parte5.transform.localRotation = new Quaternion(90.0f, 180f, 0, 0);
			parte5.transform.localScale = Vector3.one;
		}
		#endregion

		Destroy(parte1.gameObject, 4.0f);
		parte1 = null;
		Destroy(parte2.gameObject, 4.0f);
		parte2 = null;
		Destroy(parte3.gameObject, 4.0f);
		parte3 = null;
		Destroy(parte4.gameObject, 4.0f);
		parte4 = null;
		Destroy(parte5.gameObject, 4.0f);
		parte5 = null;
	}

	// Update is called once per frame
	void Update ()
	{

		if(Input.GetKeyDown(KeyCode.Y))
		{
			InstanciarConfetti(4.0f);
//			foreach (Transform partesConffeti in conffeti.transform)
//			{
//				Debug.LogError("Partes Confetti: " + partesConffeti);
//				partesConffeti.gameObject.SetActive(true);
//				emisor = partesConffeti.GetComponent<ParticleEmitter>();
//				
//				emisor.ClearParticles();
//				emisor.Emit();
//				Debug.LogError("Emisor: " + emisor);
//				
//			}
		}
			


		if(estaEnCustomizar)
		{
			if(PlayerPrefsX.GetBool("PrimeraVezPasar") == true)
			{
				if(restricciones[0] == true &&
				   restricciones[1] == true &&
				   restricciones[2] == true &&
				   restricciones[3] == true &&
				   restricciones[4] == true &&
				   restricciones[5] == true)
				{
					botonSiguiente.isEnabled = true;
					pasar = true;


					//CargarParticles();
						Debug.LogError("<------------------------------ Deberia aparecer este debug asi cambie de escena ------------------------------>");
					InstanciarConfetti(3.0f);
//					PlayerPrefsX.SetBool("Confetti", true);
				}
			}
			else
			{
				botonSiguiente.isEnabled  = true;
				pasar = true;

//				InstanciarConfetti(3.0f);
//				unaVez = true;
			}
		}

		partsToReceive[1].enabled = true;
		partsToReceive[3].enabled = true;
		partsToReceive[4].enabled = true;

		for (int i = 0; i < partsToReceive.Length; i++)
		{
			partsToReceive[i].spriteName = PlayerPrefs.GetString(keys[i]);

		}

		if(!actualizarTexto)
			textoActual.text = PlayerPrefs.GetString("textoActual");
	
	}
}
