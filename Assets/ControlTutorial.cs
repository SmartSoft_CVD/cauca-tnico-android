﻿using UnityEngine;
using System.Collections;

public class ControlTutorial : MonoBehaviour {

	public UIButton openTuto;
	public UIButton closeTuto;
	private BoxCollider boxCollider;
	public bool pausedGame;
	public bool estaEnJuego = false;

	void Awake()
	{
		boxCollider = this.gameObject.GetComponent<BoxCollider>();
	}

	// Use this for initialization
	void Start ()
	{
		boxCollider.enabled = false;

		EventDelegate.Add(openTuto.onClick, delegate()
  		{
			boxCollider.enabled = true;
			pausedGame = true;
			Time.timeScale = 0;
		});

		EventDelegate.Add(closeTuto.onClick, delegate()
  		{
			boxCollider.enabled = false;
			pausedGame = false;
			Time.timeScale = 1;
		});

		if(estaEnJuego)
			openTuto.gameObject.SendMessage("OnClick");
	}
}
