﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ClampFeatures : MonoBehaviour
{
	public List<GameObject> features = new List<GameObject>();
	public GameObject[] objs;
	public GameObject[] objsNoSelected;
	public UIButton buttonContinue;
	public GameObject [] threeLabels;
	public string [] threeImages;
	public string[] mensajes;
	// Use this for initialization
	void Start ()
	{
		mensajes = new string[3];
		threeLabels = new GameObject[3];
		threeImages = new string[3];

		buttonContinue = GameObject.Find("DarPersonalidad/ButtonNext").GetComponent<UIButton>();
		buttonContinue.isEnabled = false;

		foreach (Transform child in this.transform)
		{
			foreach (Transform granchild in child.transform)
			{
				if(granchild.gameObject.GetComponent<UIButton>() != null)
				{
					features.Add(granchild.gameObject);

					EventDelegate.Add(granchild.GetComponent<UIToggle>().onChange, delegate ()
					                  {


						if(UIToggle.current.value)
							granchild.gameObject.name = "Escogido";
						else
							granchild.gameObject.name = "NoEscogido";


						metodo();
						
					});

				}
			}
		}



	}

	void metodo ()
	{
		objs = GameObject.FindObjectsOfType(typeof(GameObject)).Select(g => g as GameObject).Where(g => g.name == "Escogido").ToArray();
		objsNoSelected = GameObject.FindObjectsOfType(typeof(GameObject)).Select(g => g as GameObject).Where(g => g.name == "NoEscogido").ToArray();


		if(objs.Length == 3)
		{
			buttonContinue.isEnabled = true;
			for (int v = 0; v < objsNoSelected.Length; v++)
			{
				objsNoSelected[v].GetComponent<UIButton>().isEnabled = false;
				
			}
		}
		else
		{
			buttonContinue.isEnabled = false;
			for (int y = 0; y < objsNoSelected.Length; y++)
			{
				objsNoSelected[y].GetComponent<UIButton>().isEnabled = true;
				
			}
		}

		for (int r = 0; r < 3; r++)
		{
			if(objs[r].name == "Escogido")
			{
				threeLabels [r] = objs[r].transform.parent.FindChild("LabelDescription").gameObject;
				threeImages [r] = objs[r].transform.FindChild("Background").GetComponent<UISprite>().spriteName; 
			}

		}


		for (int a = 0; a < threeLabels.Length; a++)
		{
			mensajes[a] = threeLabels[a].GetComponent<UILabel>().text;

		}

		PlayerPrefsX.SetStringArray("Features", mensajes);
		PlayerPrefsX.SetStringArray("ImagesFeatures", threeImages);


	}

	void OnChange()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{


	}
}
