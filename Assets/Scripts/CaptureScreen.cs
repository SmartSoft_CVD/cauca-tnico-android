﻿using UnityEngine;
using System.Collections;

public class CaptureScreen : MonoBehaviour {

	public GameObject boton;

	// Use this for initialization
	void Start ()
	{
		UIEventListener.Get(boton).onClick += delegate(GameObject go) {
			NGUITools.SetActive(boton, false);
			Application.CaptureScreenshot("imagen.png");
		};
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}



}
