﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConfirmName : MonoBehaviour {

	public List<GameObject> cards;
	public List<GameObject> cardsPressed;

	public GameObject carta;

	public GameObject front;
	public GameObject back;

	public int numeroParejas = 0;
	bool gano = false;

	bool forward = false;

	public RuntimeAnimatorController controlCarta_1;
	public RuntimeAnimatorController controlCarta_2;

	void Awake()
	{
		cards = new List<GameObject>();
		
		foreach (Transform item in transform)
		{
			cards.Add(item.gameObject);
		}
	}
	void Start()
	{
		//UIEventListener.Get(carta).onClick += OtroMetodo;
		foreach (GameObject currentCard in cards)
		{
			UIEventListener.Get(currentCard).onClick += MetodoPresionarBoton;
		}
	}

	public void OtroMetodo(GameObject f)
	{
//		f.GetComponent<Animation>().Play("carta");

	}

	public void MetodoPresionarBoton(GameObject gO)
	{
		gO.GetComponent<UIButton>().isEnabled = false;
		// front : 1
		// back : 0
		gO.transform.GetChild(0).GetComponent<TweenRotation>().PlayForward();
		//front.GetComponent<TweenRotation>().PlayForward();
		



		EventDelegate.Add(gO.transform.GetChild(0).GetComponent<TweenRotation>().onFinished, delegate()

			                  {

			if(gO.transform.GetChild(0).GetComponent<TweenRotation>().direction == AnimationOrTween.Direction.Forward)
			{
				gO.transform.GetChild(1).GetComponent<TweenRotation>().PlayForward();
			}

			});

			cardsPressed.Add(gO);

			if(cardsPressed.Count == 2)
			{
				

				if(cardsPressed[0].name == cardsPressed[1].name)
				{
//					Debug.Log("Pareja");
					Destroy(cardsPressed[0].GetComponent<Collider>());
					Destroy(cardsPressed[1].GetComponent<Collider>());
					cardsPressed.Clear();

					foreach (GameObject cardCurrent in cards)
					{
						if(cardCurrent.GetComponent<Collider>() != null)
							cardCurrent.GetComponent<UIButton>().isEnabled = true;
						
					}
					numeroParejas+=1;
				}
				else
				{
//					Debug.Log("Esta no es la Pareja");

					foreach (GameObject cardCurrent in cards)
					{
						cardCurrent.GetComponent<UIButton>().isEnabled = false;

					}


					EventDelegate.Add(gO.transform.FindChild("Front").GetComponent<TweenRotation>().onFinished, delegate() {
					if(gO.transform.FindChild("Front").GetComponent<TweenRotation>().direction == AnimationOrTween.Direction.Forward)
						MetodoAnim();
				});




				}
			}
			

	}
	
	void Update()
	{
		if(numeroParejas == 2)
		{
			for (int m = 0; m < cards.Count; m++) {
				cards[m].GetComponent<UIButton>().isEnabled = false;
			}
		}
//		if(Input.GetKeyDown(KeyCode.C))
//		{
//			back.GetComponent<TweenRotation>().PlayReverse();
//
//
//				EventDelegate.Add(back.GetComponent<TweenRotation>().onFinished, delegate()
//			                  {
//				if(back.GetComponent<TweenRotation>().direction == AnimationOrTween.Direction.Reverse)
//				{
//					front.GetComponent<TweenRotation>().PlayReverse();
//				}
//					});
//
//		}
	}
	


	void MetodoAnim()
	{
		StartCoroutine(ReproducirAnim());
	}
	
	IEnumerator ReproducirAnim()
	{
//		Debug.Log("Voltear las tarjetas");

		yield return new WaitForSeconds(0.2f);

		cardsPressed[0].AddComponent<Animator>().enabled = true;
		cardsPressed[1].AddComponent<Animator>().enabled = true;

		cardsPressed[0].GetComponent<Animator>().runtimeAnimatorController = controlCarta_1;
		cardsPressed[1].GetComponent<Animator>().runtimeAnimatorController = controlCarta_2;


		yield return new WaitForSeconds(0.5f);
		
		cardsPressed[0].transform.FindChild("Back").GetComponent<TweenRotation>().PlayReverse();
		
		EventDelegate.Add(cardsPressed[0].transform.FindChild("Back").GetComponent<TweenRotation>().onFinished, delegate()
		                  {
			if(cardsPressed[0].transform.FindChild("Back").GetComponent<TweenRotation>().direction == AnimationOrTween.Direction.Reverse)
			{
				cardsPressed[0].transform.FindChild("Front").GetComponent<TweenRotation>().PlayReverse();
			}
		});

		cardsPressed[1].transform.FindChild("Back").GetComponent<TweenRotation>().PlayReverse();

		EventDelegate.Add(cardsPressed[1].transform.FindChild("Back").GetComponent<TweenRotation>().onFinished, delegate()
		                  {
			if(cardsPressed[1].transform.FindChild("Back").GetComponent<TweenRotation>().direction == AnimationOrTween.Direction.Reverse)
			{
				cardsPressed[1].transform.FindChild("Front").GetComponent<TweenRotation>().PlayReverse();
			}
		});


		yield return new WaitForSeconds(0.25f);

		Destroy(cardsPressed[0].GetComponent<Animator>());
		Destroy(cardsPressed[1].GetComponent<Animator>());

//		Debug.Log("Aqui debe finalizar");

		foreach (GameObject cardCurrent in cards)
		{
			if(cardCurrent.GetComponent<Collider>() != null)
			cardCurrent.GetComponent<UIButton>().isEnabled = true;
			
		}

		yield return new WaitForEndOfFrame();
		cardsPressed.Clear();
		
	}
	
}
