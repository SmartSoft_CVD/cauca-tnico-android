﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;

public class FacebookControllerShareAvatar : MonoBehaviour {

	#region FB.Init() example

	public Texture2D textura;
	private bool isInit = false;
	private string lastResponse;
	private Texture2D lastResponseTexture;
	public string ApiQuery = "";

	public TweenScale tweenPos;
	public GameObject buttonClose;

	public GameObject textoPostFacebook;
	private TweenPosition tweenPosTextFacebook;
	private UIInput textoPostFacebookComponente;

	public GameObject confirmarTexto;
	public UIButton bFacebook;

	public GameObject [] objetosAMostrar;
	public GameObject [] objetosAOcultar;

	public string [] labels;

	public GameObject [] botones;

	TweenPosition tP1;
	TweenPosition tP2;

	public GameObject avatarTerminado;
	public GameObject avatarParent;
//	public UILabel labelLogin;

	void Awake()
	{
	}



	// Use this for initialization
	void Start ()
	{


		labels = PlayerPrefsX.GetStringArray("Features");

//		tweenPosTextFacebook = textoPostFacebook.GetComponent<TweenPosition>();
//		textoPostFacebookComponente = textoPostFacebook.GetComponent<UIInput>();
//		NGUITools.SetActive(buttonClose, false);




		EventDelegate.Add(bFacebook.onClick,  delegate()
		                  { if(!FB.IsLoggedIn) { CallFBLogin(); }
			else { StartCoroutine(TakeScreenshot()); /*labelLogin.text = "Se logueo logueado";*/ } });
//		if(tweenPos.direction == AnimationOrTween.Direction.Forward)
//			EventDelegate.Add(tweenPos.onFinished, delegate()
//			                  { if(!FB.IsLoggedIn) { CallFBLogin(); }
//				else { StartCoroutine(TakeScreenshot()); /*labelLogin.text = "Se logueo logueado";*/ } });
	}



	
	#endregion

	private void CallFBLogin ()
	{
		FB.Login ("email,publish_actions", LoginCallback);

	}

	void LoginCallback (FBResult result)
	{
//		if (result.Error != null){
//			lastResponse = "Error Response:\n" + result.Error;
//			labelLogin.text = "Hubo un error en la API";
//		}
//		else if(FB.IsLoggedIn){
//			lastResponse = "Login was successful!";
//			
//			StartCoroutine(TakeScreenshot());
//			
//			labelLogin.text = "Esta logueado";
//			
//		}
//		else if (!FB.IsLoggedIn) {
//			lastResponse = "Login cancelled by Player";
//			labelLogin.text = "No esta logueado";
//		} 

		if(result.Error != null)
		{
			if (avatarTerminado != null) {
				
				// Desactivamos la pantalla actual
				avatarTerminado.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (avatarTerminado);
				avatarTerminado = null;
			}
			
			avatarTerminado = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Error/LabelErrorPost"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			avatarTerminado.name = "Error Post";
			
			// Lo agregamos como hijo del HUD
			avatarTerminado.transform.parent = avatarParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			avatarTerminado.transform.localPosition = Vector3.zero;
			avatarTerminado.transform.localRotation = Quaternion.identity;
			avatarTerminado.transform.localScale = Vector3.one;
			
			Destroy(avatarTerminado, 5.0f);
		}
		else
		{
			StartCoroutine(TakeScreenshot());
		}

	}

	private IEnumerator TakeScreenshot ()
	{
//		foreach (GameObject itemOcultar in objetosAOcultar)
//		{
//			NGUITools.SetActive(itemOcultar, false);
//		}
//
//		foreach (GameObject itemMostrar in objetosAMostrar)
//		{
//			NGUITools.SetActive(itemMostrar, true);
//		}
		if(FB.IsLoggedIn)
		{

	//		NGUITools.SetActive(botones[0], false);
	//		NGUITools.SetActive(botones[1], false);
			#region Animación botón facebook
			// Animación botón facebook
			tP1 = TweenPosition.Begin(botones[0], 0.1f, new Vector3(-374.94f, -543f, 0));
			tP1.from = botones[0].gameObject.transform.localPosition;
			tP1.to = new Vector3(-374.94f,  -665.34f, 0);
			tP1.PlayForward();
			#endregion
			
			#region Animación botón facebook
			// Animación botón facebook
			tP2 = TweenPosition.Begin(botones[1], 0.1f, new Vector3(690f, 563f, 0));
			tP2.from = botones[1].gameObject.transform.localPosition;
			tP2.to = new Vector3(690f,  685f, 0);
			tP2.PlayForward();
			#endregion


			yield return new WaitForSeconds(1.0f);

			yield return new WaitForEndOfFrame();
			//if (GUI.Button(new Rect(0.08f * Screen.width, 0.14f * Screen.height, 0.84f * Screen.width,  0.73f * Screen.height),"Click"))

			var width = Screen.width * 0.84f;
			var height = Screen.height * 0.73f;
			var tex = new Texture2D ((int)width, (int)height, TextureFormat.RGB24, false);
			// Read screen contents into the texture
			tex.ReadPixels (new Rect (Screen.width * 0.08f, Screen.height * 0.133f, width, height), 0, 0);
			tex.Apply ();

			

	//		TweenPosition tP = TweenPosition.Begin(textoPostFacebook, 0.5f, new Vector3(0, -243f, 0));
	//		tP.from = textoPostFacebook.gameObject.transform.localPosition;

			byte[] screenshot = tex.EncodeToPNG ();
			var wwwForm = new WWWForm ();
			wwwForm.AddBinaryData ("image", screenshot, "FotoCaucaEtnico.png");
	//		wwwForm.AddField ("message", "1. " + labels[0] + " 2. " + labels[1] + " 3. " + labels[2]);

			wwwForm.AddField ("message", "Descarga y juega Cauca Étnico. Conoce acerca de las Étnias del territorio Caucano");

			FB.API ("me/photos", Facebook.HttpMethod.POST, Callback, wwwForm);
	//		tP.PlayReverse();
	//		NGUITools.SetActive(buttonClose, true);
			
			Debug.Log("ya hizo la publicacion");
		}




//		foreach (GameObject itemOcultar in objetosAOcultar)
//		{
//			NGUITools.SetActive(itemOcultar, true);
//		}
//		
//		foreach (GameObject itemMostrar in objetosAMostrar)
//		{
//			NGUITools.SetActive(itemMostrar, false);
//		}

//		UIEventListener.Get(confirmarTexto).onClick += delegate
//		{
////			byte[] screenshot = tex.EncodeToPNG ();
////			var wwwForm = new WWWForm ();
////			wwwForm.AddBinaryData ("image", screenshot, "FotoCaucaEtnico.png");
////			wwwForm.AddField ("message", textoPostFacebookComponente.label.text);
////
////			FB.API ("me/photos", Facebook.HttpMethod.POST, Callback, wwwForm);
////			tP.PlayReverse();
////			NGUITools.SetActive(buttonClose, true);
////
////			Debug.Log("ya hizo la publicacion");
//
//		};


		
		
	}

	void Callback (FBResult result)
	{
		if(result.Error == null)
		{
			tP1.PlayReverse();
			tP2.PlayReverse();

			botones[0].GetComponent<UIButton>().isEnabled = false;

			ShowFeatures.ocultarMostrar = false;
		}
		else
		{
			if (avatarTerminado != null) {
				
				// Desactivamos la pantalla actual
				avatarTerminado.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (avatarTerminado);
				avatarTerminado = null;
			}

			avatarTerminado = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Error/LabelErrorPost"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			avatarTerminado.name = "Error Post";
			
			// Lo agregamos como hijo del HUD
			avatarTerminado.transform.parent = avatarParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			avatarTerminado.transform.localPosition = Vector3.zero;
			avatarTerminado.transform.localRotation = Quaternion.identity;
			avatarTerminado.transform.localScale = Vector3.one;

			Destroy(avatarTerminado, 5.0f);

			tP1.PlayReverse();
			tP2.PlayReverse();
			
		}
//		if(result.Error == null)
//		{
//			foreach (GameObject itemOcultar in objetosAOcultar)
//			{
//				NGUITools.SetActive(itemOcultar, true);
//			}
//			
//			foreach (GameObject itemMostrar in objetosAMostrar)
//			{
//				NGUITools.SetActive(itemMostrar, false);
//			}
//		}
//		lastResponseTexture = null;
//		// Some platforms return the empty string instead of null.
//		if (!String.IsNullOrEmpty (result.Error))
//			lastResponse = "Error Response:\n" + result.Error;
//		else if (!ApiQuery.Contains ("/picture"))
//			lastResponse = "Success Response:\n" + result.Text;
//		else {
//			lastResponseTexture = result.Texture;
//			lastResponse = "Success Response:\n";
//		}
	}


//	void OnGUI()
//	{
//		if (GUI.Button(new Rect(0.08f * Screen.width, 0.14f * Screen.height, 0.84f * Screen.width,  0.73f * Screen.height),"Click"))
//		{
//
//		}
//
//	}

	
	// Update is called once per frame
	void Update ()
	{
//		if(FB.IsLoggedIn)
//			labelLogin.text = "Esta logueado";
//		else
//			labelLogin.text = "No esta logueado";
	
	}
}
