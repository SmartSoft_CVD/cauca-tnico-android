﻿using UnityEngine;
using System.Collections;

public enum eScreen
{
	/* Este enum almacenara las diferentes pantallas del juego
	 * NOTA: una pantalla puede estar conformada por varios prefabs */

	NONE,
	// Logo
	LOGO,

	// Tutorial
	TUTORIAL,

	// Selección
	SELECTION,

	// Escoger Avatar
	ESCOGER_AVATAR,

	// Crear Avatar
	CREAR_AVATAR,

	// Características
	DESCRIPTION,

	// Mini juego inicio
	MINIJUEGO_INICIO,

	// Felicitaciones
	CONGRATULATIONS,

	// Avatar
	AVATAR,

	// Dar personalidad
	GIVE_PERSONALITY,

	// Dar personalidad (Texto 1)
	PERSONALITY_TEXT1,

	// Dar personalidad (Texto 2)
	PERSONALITY_TEXT2,

	// Avatar terminado
	FINISHED_AVATAR,

	// Compartir Avatar
	SHARE_AVATAR,

	// Capturar Avatar
	CAPTURE_AVATAR,

	/* En caso de que existan mas pantallas en el juego se
	 * deberán ir agregando como parte de las constantes de este enum */
}
