﻿using UnityEngine;
using System.Collections;

public class TimeBonus : MonoBehaviour {

	public float timeBonus;
	public int minimunTime;
	public int maximunTime;
	public UISprite clockProgress;
	public float fill;

	private GameObject barajarObj;
	private Bajarar barajarComp;



	private string root = "UI RootNotFixed/Camera/Anchor/Barajar";

	void Start()
	{
		barajarObj = GameObject.Find(root);

		if(barajarObj != null)
			barajarComp = barajarObj.GetComponent<Bajarar>();
		else
			Debug.LogWarning("Esat vacio");

		timeBonus = GiveTime();
		StartCoroutine(Recharge());
	}
	public float GiveTime()
	{
		float timeRandom = Random.Range(minimunTime, maximunTime);
		return timeRandom;
	}

	public float GiveFill()
	{
		float fillValue = fill;
		return fillValue;
	}

	void Update()
	{
		clockProgress.fillAmount = fill;		 
	}
	
	IEnumerator Recharge() 
	{        
		float t = timeBonus;
		fill = 0f;
		
		while (t > 0)
		{
			fill = Mathf.Lerp(0f, 1f, t/timeBonus); //if it's between 0 and 1, you could just have t/myAnimationDuration instead of the whole lerp.
			t -= Time.deltaTime;      
			yield return null; //makes the coroutine stop this frame and return next frame to continue by coroutine magic.
		}
		//make sure that you hit 100 % and not more after the loop is done
		fill = 0; //lerp may clamp this already, but just to make sure.
		Debug.LogWarning("Destruir ya que se acabo el tiempo");

		GameObject cardWithTag = GameObject.FindGameObjectWithTag("PowerUp");
		cardWithTag.tag = "Untagged";

		yield return new WaitForEndOfFrame();

		Destroy(this.gameObject);

		barajarComp.executePowerUp = false;



	}
}
