﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hud
{
	#region Variables
	// Ruta del objeto "HUD" en la jerarquia 
	public static string rootHUD = "UI Root/Camera/HUD/";
	// Referencia al objeto "HUD" en la jerarquia
	private static GameObject hudMainParent = GameObject.Find (rootHUD);

	private static bool guardarAvatar = false;

	// Listas de Prefab 
	private static List<ePrefab> requestedPrefabs = new List<ePrefab> ();
	private static List<ePrefab> loadedPrefabs = new List<ePrefab> ();

	// Esatdos
	private static eScreen hudCurrentState = eScreen.NONE;
	private static bool isSwitchingState = false;

	// Referencias a los Prefabs (Parents)

	// - LG: Logo
	private static GameObject hudLG_Parent;

	// - TU: Tutorial
	private static GameObject hudTU_Parent;

	// - SE: Selección
	private static GameObject hudSE_Parent;

	// - EA: Escoger Avatar
	private static GameObject hudEA_Parent;

	// - CRA: Crear Avatar
	private static GameObject hudCRA_Parent;

	// - AV: Avatar
	public static GameObject hudAV_Parent;

	// - DE: Descripción
	private static GameObject hudDE_Parent;

	// - MI: Minijuego Inicio
	private static GameObject hudMI_Parent;

	// - CO: Felicitaciones
	private static GameObject hudCO_Parent;

	// - GP: Dar Personalidad
	private static GameObject hudGP_Parent;

	// - PT1: Dar Personalidad (Text 1)
	private static GameObject hudPT1_Parent;

	// - PT2: Dar Personalidad (Text 2)
	private static GameObject hudPT2_Parent;

	// - FA: Avatar Cmpleto
	private static GameObject hudFA_Parent;

	// - SA: Compartir Avatar
	private static GameObject hudSA_Parent;

	// - CA: Capturar Avatar
	private static GameObject hudCA_Parent;
	#endregion


	#region ->  switchToState()  <-
	public static void switchToState (eScreen newState)
	{
		rootHUD = "UI Root/Camera/HUD/";
		// Referencia al objeto "HUD" en la jerarquia
		hudMainParent = GameObject.Find (rootHUD);

			// Si el nuevo estado es diferente al actual y no se esta ya en un proceso de cambio de HUD...
		if ((newState != hudCurrentState) && (isSwitchingState == false))
		{
			// Activamos la bandera que indica el inicio de un proceso de cambio de HUD
			isSwitchingState = true;

			// Reiniciamos la lista de prefabs a cargar
			requestedPrefabs.Clear ();

			// Asignamos el nuevo estado
			hudCurrentState = newState;

			//Paso 3:// Agregamos a las listas respectivas los HUDs que deseamos activar y que pertenecen solo al nuevo HUD State
			switch (hudCurrentState)
			{
				case eScreen.LOGO:
					requestedPrefabs.Add (ePrefab.LOGO_PREFAB);				
					break;

				case eScreen.TUTORIAL:
					requestedPrefabs.Add (ePrefab.TUTORIAL_PREFAB);				
					break;

				case eScreen.SELECTION:
					requestedPrefabs.Add (ePrefab.SELECTION_PREFAB);					
					break;

				case eScreen.ESCOGER_AVATAR:
					requestedPrefabs.Add (ePrefab.ESCOGER_AVATAR_PREFAB);					
					break;

				case eScreen.CREAR_AVATAR:
					requestedPrefabs.Add (ePrefab.CREAR_AVATAR_PREFAB);					
					break;

				case eScreen.AVATAR:
					requestedPrefabs.Add (ePrefab.AVATAR_PREFAB);
					break;

				case eScreen.DESCRIPTION:
					requestedPrefabs.Add (ePrefab.DESCRIPTION_PREFAB);
					break;

				case eScreen.MINIJUEGO_INICIO:
					requestedPrefabs.Add (ePrefab.MINIJUEGO_INICIO_PREFAB);
					break;

				case eScreen.CONGRATULATIONS:
					requestedPrefabs.Add (ePrefab.CONGRATULATIONS_PREFAB);					
					break;

				case eScreen.GIVE_PERSONALITY:
					requestedPrefabs.Add (ePrefab.GIVE_PERSONALITY_PREFAB);					
					break;

				case eScreen.PERSONALITY_TEXT1:
					requestedPrefabs.Add (ePrefab.PERSONALITY_TEXT1_PREFAB);					
					break;

				case eScreen.PERSONALITY_TEXT2:
					requestedPrefabs.Add (ePrefab.PERSONALITY_TEXT2_PREFAB);					
					break;

				case eScreen.FINISHED_AVATAR:
					requestedPrefabs.Add (ePrefab.FINISHED_AVATAR_PREFAB);					
					break;

				case eScreen.SHARE_AVATAR:
					requestedPrefabs.Add (ePrefab.SHARE_AVATAR_PREFAB);				
					break;

				case eScreen.CAPTURE_AVATAR:
					requestedPrefabs.Add (ePrefab.CAPTURE_AVATAR_PREFAB);				
					break;
			}

			//OJO NO TOCAR// - Determinamos cuales de los prefabs a cargar no deben hacerlo, por ya estar cargados:
			List<ePrefab> prefabsToCreate = new List<ePrefab> ();

			// Recorremos la lista de prefabs a cargar...
			for (int currentRequestedPrefab = 0; currentRequestedPrefab < requestedPrefabs.Count; currentRequestedPrefab++)
			{
				bool currentRequestedPrefabExist = false;

				// Recorremos la lista de prefabs cargados actualmente...
				for (int currentLoadedPrefab = 0; currentLoadedPrefab < loadedPrefabs.Count; currentLoadedPrefab++)
				{
					// Realizamos la verificacion
					if (requestedPrefabs [currentRequestedPrefab] == loadedPrefabs [currentLoadedPrefab])
					{
						// El prefab actual no debe ser creado, ya que esta siendo solicitado nuevamente
						currentRequestedPrefabExist = true;

						// Salimos del ciclo inmediatamente...
						break;
					}
				}

				// Si el prefab actual requerido no existe...
				if (currentRequestedPrefabExist == false)
				{
					// Es necesaria su creacion...
					prefabsToCreate.Add (requestedPrefabs [currentRequestedPrefab]);
				}
			}

			// - Ahora determinamos cuales de los prefabs actualmente cargados debemos eliminar:
			List<ePrefab> prefabsToDelete = new List<ePrefab> ();

			// Recorremos la lista de prefabs cargados actualmente...
			for (int currentLoadedPrefab = 0; currentLoadedPrefab < loadedPrefabs.Count; currentLoadedPrefab++)
			{
				bool currentLoadedPrefabExist = false;

				// Recorremos la lista de prefabs a cargar...
				for (int currentRequestedPrefab = 0; currentRequestedPrefab < requestedPrefabs.Count; currentRequestedPrefab++)
				{
					// Realizamos la verificacion
					if (requestedPrefabs [currentRequestedPrefab] == loadedPrefabs [currentLoadedPrefab])
					{
						// El prefab actual no debe ser eliminado, ya que esta siendo solicitado nuevamente
						currentLoadedPrefabExist = true;

						// Salimos del ciclo inmediatamente...
						break;
					}
				}

				// Si el prefab actual cargado no es requerido...
				if (currentLoadedPrefabExist == false)
				{
					// Es necesaria su eliminacion...
					prefabsToDelete.Add (loadedPrefabs [currentLoadedPrefab]);
				}
			}

			// Eliminamos los prefabs que no son solicitados...
			for (int index = 0; index < prefabsToDelete.Count; index ++)
			{
				deletePrefab (prefabsToDelete [index]);
			}

			// Creamos los prefabs que faltan...
			for (int index = 0; index < prefabsToCreate.Count; index ++)
			{
				loadPrefab (prefabsToCreate [index]);
			}

			// Indicamos que el cambio de HUD ha finalizado
			isSwitchingState = false;
		}
			//FIN NO TOCAR
	}
	#endregion


	#region ->  Carga de prefabs  <-    
	private static void loadPrefab (ePrefab prefab)
	{		
		// Agregamos el prefab a la lista...
		loadedPrefabs.Add (prefab);	

		switch (prefab)
		{			
			#region ->  eHudPrefab.LOGO_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
			case ePrefab.LOGO_PREFAB:
				
			// Cargamos el Prafab correspondiente
			hudLG_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Logo"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudLG_Parent.name = "Logo";
					

			// Lo agregamos como hijo del HUD
			hudLG_Parent.transform.parent = hudMainParent.transform;

					
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudLG_Parent.transform.localPosition = Vector3.zero;
			hudLG_Parent.transform.localRotation = Quaternion.identity;
			hudLG_Parent.transform.localScale = Vector3.one;


			NavigationMap.ChangeOfScreen(hudLG_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.SELECTION);
			break;
			#endregion

			#region ->  eHudPrefab.TUTORIAL_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.TUTORIAL_PREFAB:
			
			// Cargamos el Prafab correspondiente
			hudTU_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Tutorial"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudTU_Parent.name = "Tutorial";
			
			// Lo agregamos como hijo del HUD
			hudTU_Parent.transform.parent = hudMainParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudTU_Parent.transform.localPosition = Vector3.zero;
			hudTU_Parent.transform.localRotation = Quaternion.identity;
			hudTU_Parent.transform.localScale = Vector3.one;
			
			
			break;
			#endregion

			#region ->  eHudPrefab.SELECTION_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.SELECTION_PREFAB:
			
			// Cargamos el Prafab correspondiente
			hudSE_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Seleccion"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudSE_Parent.name = "Seleccion";
			
			// Lo agregamos como hijo del HUD
			hudSE_Parent.transform.parent = hudMainParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudSE_Parent.transform.localPosition = Vector3.zero;
			hudSE_Parent.transform.localRotation = Quaternion.identity;
			hudSE_Parent.transform.localScale = Vector3.one;
			
			
			NavigationMap.ChangeOfScreen(hudSE_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.ESCOGER_AVATAR);
			NavigationMap.ChangeOfScreen(hudSE_Parent.GetComponent<Widgets>().widgetsList[1], eScreen.CREAR_AVATAR);

			NavigationMap.ChangeOfScreen(hudSE_Parent.GetComponent<Widgets>().widgetsList[2], eScreen.ESCOGER_AVATAR);
			NavigationMap.ChangeOfScreen(hudSE_Parent.GetComponent<Widgets>().widgetsList[3], eScreen.CREAR_AVATAR);
			break;
			#endregion

			#region ->  eHudPrefab.ESCOGER_AVATAR_PREFAB_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.ESCOGER_AVATAR_PREFAB:
			
			// Cargamos el Prafab correspondiente
			hudEA_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Escoger_Avatar"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudEA_Parent.name = "Escoger Avatar";
			
			// Lo agregamos como hijo del HUD
			hudEA_Parent.transform.parent = hudMainParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudEA_Parent.transform.localPosition = Vector3.zero;
			hudEA_Parent.transform.localRotation = Quaternion.identity;
			hudEA_Parent.transform.localScale = Vector3.one;

			NavigationMap.ChangeOfScreen(hudEA_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.DESCRIPTION);
			NavigationMap.ChangeOfScreen(hudEA_Parent.GetComponent<Widgets>().widgetsList[1], eScreen.SELECTION);
			NavigationMap.ChangeOfScreen(hudEA_Parent.GetComponent<Widgets>().widgetsList[2], eScreen.SELECTION);
			break;
			#endregion

			#region ->  eHudPrefab.CREAR_AVATAR_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.CREAR_AVATAR_PREFAB:
			
			// Cargamos el Prafab correspondiente
			hudCRA_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Crear_Avatar"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudCRA_Parent.name = "Crear Avatar";
			
			// Lo agregamos como hijo del HUD
			hudCRA_Parent.transform.parent = hudMainParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudCRA_Parent.transform.localPosition = Vector3.zero;
			hudCRA_Parent.transform.localRotation = Quaternion.identity;
			hudCRA_Parent.transform.localScale = Vector3.one;

			PlayerPrefs.DeleteKey("Cara");
			PlayerPrefs.DeleteKey("Cabeza");
			PlayerPrefs.DeleteKey("Camisa");
			PlayerPrefs.DeleteKey("Pantalon");
			PlayerPrefs.DeleteKey("Zapatos");
			PlayerPrefs.DeleteKey("Accesorios");
			PlayerPrefs.DeleteKey("Sombreros");

			PlayerPrefsX.SetBool("PrimeraVezPasar", true);

			//Debe ir a la pantalla de customizar
			NavigationMap.ChangeOfScreen(hudCRA_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.AVATAR);
			NavigationMap.ChangeOfScreen(hudCRA_Parent.GetComponent<Widgets>().widgetsList[1], eScreen.SELECTION);
			NavigationMap.ChangeOfScreen(hudCRA_Parent.GetComponent<Widgets>().widgetsList[2], eScreen.SELECTION);

			PartsReceived.unaVez = false;
			//PlayerPrefsX.SetBool("Confetti", false);
			

			guardarAvatar = false;
			break;
			#endregion

			#region ->  eHudPrefab.AVATAR_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.AVATAR_PREFAB:
			
			// Cargamos el Prafab correspondiente
			hudAV_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Customizar"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudAV_Parent.name = "Customizar Avatar";
			
			// Lo agregamos como hijo del HUD
			hudAV_Parent.transform.parent = hudMainParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudAV_Parent.transform.localPosition = Vector3.zero;
			hudAV_Parent.transform.localRotation = Quaternion.identity;
			hudAV_Parent.transform.localScale = Vector3.one;

//			Debug.LogWarning("Estoy en la pantalla de avatar o costumizar");	

			guardarAvatar = false;

			if(guardarAvatar)
			{

			}

			PlayerPrefs.DeleteKey("textoActual");


			NavigationMap.ChangeOfScreen(hudAV_Parent.GetComponent<Widgets>().widgetsList[1], eScreen.CREAR_AVATAR);



			break;
			#endregion

			#region ->  eHudPrefab.DESCRIPTION_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.DESCRIPTION_PREFAB:
			
			// Cargamos el Prafab correspondiente
			hudDE_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/DESCRIPTION_PREFAB"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudDE_Parent.name = "Descripcion";
			
			// Lo agregamos como hijo del HUD
			hudDE_Parent.transform.parent = hudMainParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudDE_Parent.transform.localPosition = Vector3.zero;
			hudDE_Parent.transform.localRotation = Quaternion.identity;
			hudDE_Parent.transform.localScale = Vector3.one;

			NavigationMap.ChangeOfScreen(hudDE_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.MINIJUEGO_INICIO);
			NavigationMap.ChangeOfScreen(hudDE_Parent.GetComponent<Widgets>().widgetsList[1], eScreen.SELECTION);
	

			break;
			#endregion

			#region ->  eHudPrefab.MINIJUEGO_INICIO_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.MINIJUEGO_INICIO_PREFAB:
			
			// Cargamos el Prafab correspondiente
			hudMI_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Mini_Juego_Inicio"));

			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudMI_Parent.name = "Mini juego Inicio";
			
			// Lo agregamos como hijo del HUD
			hudMI_Parent.transform.parent = hudMainParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudMI_Parent.transform.localPosition = Vector3.zero;
			hudMI_Parent.transform.localRotation = Quaternion.identity;
			hudMI_Parent.transform.localScale = Vector3.one;

			// Ir a minijuego
			//NavigationMap.ChangeOfScreen(hudMI_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.GIVE_PERSONALITY);
			break;
			#endregion

			#region ->  eHudPrefab.CONGRATULATIONS_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.CONGRATULATIONS_PREFAB:
			
			// Cargamos el Prafab correspondiente
			hudCO_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Felicitaciones"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudCO_Parent.name = "Felicitaciones";
			
			// Lo agregamos como hijo del HUD
			hudCO_Parent.transform.parent = hudMainParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudCO_Parent.transform.localPosition = Vector3.zero;
			hudCO_Parent.transform.localRotation = Quaternion.identity;
			hudCO_Parent.transform.localScale = Vector3.one;

			NavigationMap.ChangeOfScreen(hudCO_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.GIVE_PERSONALITY);
			NavigationMap.ChangeOfScreen(hudCO_Parent.GetComponent<Widgets>().widgetsList[1], eScreen.DESCRIPTION);
			break;
			#endregion

			#region ->  eHudPrefab.GIVE_PERSONALITY_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.GIVE_PERSONALITY_PREFAB:

			Debug.Log("Estoy en lapantalla de dar personalidad");
			// Cargamos el Prafab correspondiente
			hudGP_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Seleccionar2"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudGP_Parent.name = "DarPersonalidad";
			
			// Lo agregamos como hijo del HUD
			hudGP_Parent.transform.parent = hudMainParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudGP_Parent.transform.localPosition = Vector3.zero;
			hudGP_Parent.transform.localRotation = Quaternion.identity;
			hudGP_Parent.transform.localScale = Vector3.one;

			PlayerPrefsX.SetBool("PrimeraVezPasar", false);

			NavigationMap.ChangeOfScreen(hudGP_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.AVATAR);
			NavigationMap.ChangeOfScreen(hudGP_Parent.GetComponent<Widgets>().widgetsList[1], eScreen.FINISHED_AVATAR);

			guardarAvatar = true;
			break;
			#endregion

			#region ->  eHudPrefab.PERSONALITY_TEXT1_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.PERSONALITY_TEXT1_PREFAB:
			
			// Cargamos el Prafab correspondiente
			hudPT1_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Personalidad_Texto1"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudPT1_Parent.name = "PersonalidadTexto1";
			
			// Lo agregamos como hijo del HUD
			hudPT1_Parent.transform.parent = hudMainParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudPT1_Parent.transform.localPosition = Vector3.zero;
			hudPT1_Parent.transform.localRotation = Quaternion.identity;
			hudPT1_Parent.transform.localScale = Vector3.one;

			NavigationMap.ChangeOfScreen(hudPT1_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.PERSONALITY_TEXT2);
			break;
			#endregion

			#region ->  eHudPrefab.PERSONALITY_TEXT2_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.PERSONALITY_TEXT2_PREFAB:
			
			// Cargamos el Prafab correspondiente
			hudPT2_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Personalidad_Texto2"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudPT2_Parent.name = "PersonalidadTexto2";
			
			// Lo agregamos como hijo del HUD
			hudPT2_Parent.transform.parent = hudMainParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudPT2_Parent.transform.localPosition = Vector3.zero;
			hudPT2_Parent.transform.localRotation = Quaternion.identity;
			hudPT2_Parent.transform.localScale = Vector3.one;

			NavigationMap.ChangeOfScreen(hudPT2_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.FINISHED_AVATAR);
			break;
			#endregion

			#region ->  eHudPrefab.FINISHED_AVATAR_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.FINISHED_AVATAR_PREFAB:
			
			// Cargamos el Prafab correspondiente
			hudFA_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatar_Terminado"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudFA_Parent.name = "AvatarTerminado";
			
			// Lo agregamos como hijo del HUD
			hudFA_Parent.transform.parent = hudMainParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudFA_Parent.transform.localPosition = Vector3.zero;
			hudFA_Parent.transform.localRotation = Quaternion.identity;
			hudFA_Parent.transform.localScale = Vector3.one;

			NavigationMap.ChangeOfScreen(hudFA_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.MINIJUEGO_INICIO);
			NavigationMap.ChangeOfScreen(hudFA_Parent.GetComponent<Widgets>().widgetsList[2], eScreen.SELECTION);

			// Aquí debe compartir en facebook
//			NavigationMap.ChangeOfScreen(hudFA_Parent.GetComponent<Widgets>().widgetsList[1], eScreen.MINIJUEGO_INICIO);
			break;
			#endregion

			#region ->  eHudPrefab.SHARE_AVATAR_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.SHARE_AVATAR_PREFAB:
			
			// Cargamos el Prafab correspondiente
			hudSA_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Compartir_Avatar"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudSA_Parent.name = "CompartirAvatar";
			
			// Lo agregamos como hijo del HUD
			hudSA_Parent.transform.parent = hudMainParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudSA_Parent.transform.localPosition = Vector3.zero;
			hudSA_Parent.transform.localRotation = Quaternion.identity;
			hudSA_Parent.transform.localScale = Vector3.one;

			NavigationMap.ChangeOfScreen(hudSA_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.FINISHED_AVATAR);
			NavigationMap.ChangeOfScreen(hudSA_Parent.GetComponent<Widgets>().widgetsList[1], eScreen.CAPTURE_AVATAR);
			break;
			#endregion

			#region ->  eHudPrefab.CAPTURE_AVATAR_PREFAB  <-
			//Paso 4 crear mi case para cada prefab de acuerdo al ejemplo
		case ePrefab.CAPTURE_AVATAR_PREFAB:
			
			// Cargamos el Prafab correspondiente
			hudCA_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Captura_Avatar"));
			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			hudCA_Parent.name = "CapturaAvatar";
			
			// Lo agregamos como hijo del HUD
			hudCA_Parent.transform.parent = hudMainParent.transform;
			
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			hudCA_Parent.transform.localPosition = Vector3.zero;
			hudCA_Parent.transform.localRotation = Quaternion.identity;
			hudCA_Parent.transform.localScale = Vector3.one;
			
			NavigationMap.ChangeOfScreen(hudCA_Parent.GetComponent<Widgets>().widgetsList[0], eScreen.FINISHED_AVATAR);
			break;
			#endregion



		}
	}
	#endregion

	#region ->  Eliminacion de Prefabs de la RAM  <-    
	private static void deletePrefab (ePrefab prefab)
	{
		// Removemos el prefab a la lista...
		loadedPrefabs.Remove (prefab);
		//Paso 6 crear un case por cada prefab 
		switch (prefab) {
			
			#region ->  eHudPrefab.LOGO_PREFAB  <-
			case ePrefab.LOGO_PREFAB:

				// - LG: Logo
				if (hudLG_Parent != null) {
					
					// Desactivamos la pantalla actual
					hudLG_Parent.SetActive (false); // (opcional)
					
					// Eliminamos HUD padre
					GameObject.Destroy (hudLG_Parent);
					hudLG_Parent = null;
				}
				break;
			#endregion

			#region ->  eHudPrefab.TUTORIAL_PREFAB  <-
		case ePrefab.TUTORIAL_PREFAB:
			
			// - LG: Logo
			if (hudTU_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudTU_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudTU_Parent);
				hudTU_Parent = null;
			}
			break;
			#endregion

			#region ->  eHudPrefab.SELECTION_PREFAB  <-
		case ePrefab.SELECTION_PREFAB:
			
			// - SE: Selección
			if (hudSE_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudSE_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudSE_Parent);
				hudSE_Parent = null;
			}
			break;
			#endregion

			#region ->  eHudPrefab.ESCOGER_AVATAR_PREFAB_PREFAB  <-
			case ePrefab.ESCOGER_AVATAR_PREFAB:
			
				// - GE: Género
				if (hudEA_Parent != null) {
					
					// Desactivamos la pantalla actual
					hudEA_Parent.SetActive (false); // (opcional)
					
					// Eliminamos HUD padre
					GameObject.Destroy (hudEA_Parent);
					hudEA_Parent = null;
				}
				break;
			#endregion

			#region ->  eHudPrefab.CREAR_AVATAR_PREFAB  <-
		case ePrefab.CREAR_AVATAR_PREFAB:
			
			// - GE: Género
			if (hudCRA_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudCRA_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudCRA_Parent);
				hudCRA_Parent = null;
			}
			break;
			#endregion

			#region ->  eHudPrefab.AVATAR_PREFAB  <-
			case ePrefab.AVATAR_PREFAB:
				
				// - AV: Avatar
				if (hudAV_Parent != null) {
					
					// Desactivamos la pantalla actual
					hudAV_Parent.SetActive (false); // (opcional)
					
					// Eliminamos HUD padre
					GameObject.Destroy (hudAV_Parent);
					hudAV_Parent = null;
				}
				break;
			#endregion

			#region ->  eHudPrefab.DESCRIPTION_PREFAB  <-
		case ePrefab.DESCRIPTION_PREFAB:
			
			// - FE: DESCRIPTION
			if (hudDE_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudDE_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudDE_Parent);
				hudDE_Parent = null;
			}
			break;
			#endregion

			#region ->  eHudPrefab.MINIJUEGO_INICIO_PREFAB  <-
		case ePrefab.MINIJUEGO_INICIO_PREFAB:
			
			// - FE: DESCRIPTION
			if (hudMI_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudMI_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudMI_Parent);
				hudMI_Parent = null;
			}
			break;
			#endregion

			#region ->  eHudPrefab.CONGRATULATIONS_PREFAB  <-
		case ePrefab.CONGRATULATIONS_PREFAB:
			
			// - CO: Felicitaciones
			if (hudCO_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudCO_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudCO_Parent);
				hudCO_Parent = null;
			}
			break;
			#endregion

			#region ->  eHudPrefab.CONGRATULATIONS_PREFAB  <-
		case ePrefab.GIVE_PERSONALITY_PREFAB:
			
			// - GP: Dar Personalidad
			if (hudGP_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudGP_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudGP_Parent);
				hudGP_Parent = null;
			}
			break;
			#endregion

			#region ->  eHudPrefab.PERSONALITY_TEXT1_PREFAB  <-
		case ePrefab.PERSONALITY_TEXT1_PREFAB:
			
			// - PT1: Personalidad Texto 1
			if (hudPT1_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudPT1_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudPT1_Parent);
				hudPT1_Parent = null;
			}
			break;
			#endregion

			#region ->  eHudPrefab.PERSONALITY_TEXT2_PREFAB  <-
		case ePrefab.PERSONALITY_TEXT2_PREFAB:
			
			// - PT2: Personalidad Texto 2
			if (hudPT2_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudPT2_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudPT2_Parent);
				hudPT2_Parent = null;
			}
			break;
			#endregion

			#region ->  eHudPrefab.FINISHED_AVATAR_PREFAB  <-
		case ePrefab.FINISHED_AVATAR_PREFAB:
			
			// - FA: Avatar Termnado
			if (hudFA_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudFA_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudFA_Parent);
				hudFA_Parent = null;
			}
			break;
			#endregion

			#region ->  eHudPrefab.SHARE_AVATAR_PREFAB  <-
		case ePrefab.SHARE_AVATAR_PREFAB:
			
			// - SA: Compartir Avatar
			if (hudSA_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudSA_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudSA_Parent);
				hudSA_Parent = null;
			}
			break;
			#endregion

			#region ->  eHudPrefab.CAPTURE_AVATAR_PREFAB  <-
		case ePrefab.CAPTURE_AVATAR_PREFAB:
			
			// - CA: Capture Avatar
			if (hudCA_Parent != null) {
				
				// Desactivamos la pantalla actual
				hudCA_Parent.SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (hudCA_Parent);
				hudCA_Parent = null;
			}
			break;
			#endregion
		}
	}
	#endregion
}
