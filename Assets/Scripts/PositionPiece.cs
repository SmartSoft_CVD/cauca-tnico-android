﻿using UnityEngine;
using System.Collections;

public class PositionPiece : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public IEnumerator PosicionarPieza(GameObject thisGO, GameObject hitGO)
	{
		Debug.Log("La pieza esta BIEN ubicada");
		thisGO.gameObject.transform.position = Vector3.Lerp(thisGO.gameObject.transform.position, hitGO.collider.gameObject.transform.position, 10.0f * Time.deltaTime);
		
		yield return new WaitForFixedUpdate();

		Destroy(thisGO.gameObject.GetComponent<UIDragObject>());
		thisGO.gameObject.GetComponent<PlaceCorrect>().enabled = false;
	}
}
