﻿using UnityEngine;
using System.Collections;

public class CountDown : MonoBehaviour
{
	public float t = 0; // Seconds
	public float tiempoTotal = 0;
	public UILabel clockLabel;

	public UISprite clockProgress;
	public UISprite clockProgressBase;
	public float fill;

	public bool startTime = false;

	UIWidget mWidget;
	UIWidget mWidgetBase;

	int indiceColor;

	public Color color1;
	public Color color2;
	public Color color3;

	public bool color1Bool;
	public bool color2Bool;
	public bool color3Bool;

	private bool tiempo1;
	private bool tiempo2;
	private bool tiempo3;


	void Awake()
	{
		mWidget = clockProgress.GetComponent<UIWidget>();
		mWidgetBase = clockProgressBase.GetComponent<UIWidget>();
	}
	void Start()
	{

		t = PlayerPrefs.GetFloat("TimeGame");
		startTime = true;

		if(t == 60)
		{
			tiempo1 = true;
		}

		if(t == 120)
		{
			tiempo2 = true;
		}

		if(t == 180)
		{
			tiempo3 = true;
		}

	}


	void Update()
	{
		if(startTime)
		{
			StartCoroutine(Recharge());
		}
		if(startTime && t > 0)
			t -= Time.deltaTime;

		if(tiempo1)
		{
			if(t >= 60f)
			{
				mWidget.color = color1;
				mWidgetBase.color = color1;
			}

			if(t >= 40f && t <= 60f)
			{
				mWidget.color = color1;
				mWidgetBase.color = color1;
			}
			
			if(t >= 20f && t <= 40f)
			{
				mWidget.color = color2;
				mWidgetBase.color = color2;

			}
			
			if(t >= 0f && t <= 20f)
			{
				mWidget.color = color3;
				mWidgetBase.color = color3;

			}
		}

		if(tiempo2)
		{
			if(t >= 120f)
			{
				mWidget.color = color1;
				mWidgetBase.color = color1;
			}

			if(t >= 80f && t <= 120f)
			{
				mWidget.color = color1;
				mWidgetBase.color = color1;
			}
			
			if(t >= 40f && t <= 80f)
			{
				mWidget.color = color2;
				mWidgetBase.color = color2;
				
			}
			
			if(t >= 0f && t <= 40f)
			{
				mWidget.color = color3;
				mWidgetBase.color = color3;
				
			}
		}

		if(tiempo3)
		{
			if(t >= 180f)
			{
				mWidget.color = color1;
				mWidgetBase.color = color1;
			}

			if(t >= 120f && t <= 180f)
			{
				mWidget.color = color1;
				mWidgetBase.color = color1;
			}
			
			if(t >= 60f && t <= 120f)
			{
				mWidget.color = color2;
				mWidgetBase.color = color2;
				
			}
			
			if(t >= 0f && t <= 60f)
			{
				mWidget.color = color3;
				mWidgetBase.color = color3;
				
			}
		}
	}
	
//	private string ConvertTimeToString()
//	{
//		int minutes = Mathf.FloorToInt((t % 3600) / 60);
//		int seconds = Mathf.FloorToInt(t) % 60;
//		
//		return minutes.ToString("00") + ":" + seconds.ToString("00");
//	}

	IEnumerator Recharge() 
	{


		float time = t;
		float tiempoTotal = t;
		fill = 1.0f;

//		Debug.Log("Tiempo: " + t);

		while (t > 0)
		{
//			 Mathf.Lerp(1f, 0f, time/t); //if it's between 0 and 1, you could just have t/myAnimationDuration instead of the whole lerp.
			fill = t/tiempoTotal;
			time -= Time.deltaTime;  

			clockProgress.fillAmount = fill;



			yield return null; //makes the coroutine stop this frame and return next frame to continue by coroutine magic.

//			if(clockProgress.fillAmount >= 0.7f && clockProgress.fillAmount <= 1f)
//			{
//				mWidget.color = color1;
//				Debug.LogError("Color1");
//			}
//			
//			if(clockProgress.fillAmount >= 0.3f && clockProgress.fillAmount <= 0.7f)
//			{
//				mWidget.color = color2;
//				Debug.LogError("Color2");
//			}
//			
//			if(clockProgress.fillAmount >= 0f && clockProgress.fillAmount <= 0.3f)
//			{
//				mWidget.color = color3;
//				Debug.LogError("Color3");
//			}


		}
		//make sure that you hit 100 % and not more after the loop is done
		fill = 0; //lerp may clamp this already, but just to make sure.






	}
}
