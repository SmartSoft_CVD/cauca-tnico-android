﻿using UnityEngine;
using System.Collections;

public enum Levels { NONE, facil , medio , dificil }
public class ToggleTime : MonoBehaviour {

	public UIToggle [] times;

	public TweenPosition [] tweensTables;
	public TweenScale [] tweensLabels;

	string [] partsOfName;
	string nameComplete;

	public UIButton buttonStart;

	public CountDown countDown;

	public Levels levelCurrent;

	public GameObject[] botonesNiveles;
	public float[] tiempos;

	public string rootHUD;
	// Referencia al objeto "HUD" en la jerarquia
	private GameObject hudMainParent;

	// Use this for initialization
	void Start ()
	{
//		buttonStart.isEnabled = false;

		levelCurrent = Levels.NONE;



		foreach (GameObject botonActual in botonesNiveles)
		{
			UIEventListener.Get(botonActual).onClick += SetLvl;
		}


		foreach (TweenPosition tweenCurrent in tweensTables)
		{
			tweenCurrent.method = UITweener.Method.BounceIn;
		}
		foreach (TweenScale labelCurrent in tweensLabels)
		{
			labelCurrent.method = UITweener.Method.BounceIn;
		}
	}

	void SetLvl(GameObject gameobject)
	{
		Time.timeScale = 1.0f;
		switch (gameobject.name)
		{
			case "TableHard":
				PlayerPrefs.SetFloat("TimeGame", tiempos[0]);

			PlayerPrefs.SetString("DificultadNivel", "dificil");
			levelCurrent = Levels.dificil;
				break;

		case "TableNormal":
			PlayerPrefs.SetFloat("TimeGame", tiempos[1]);
			PlayerPrefs.SetString("DificultadNivel", "medio");

			levelCurrent = Levels.medio;
			break;

		case "TableEasy":
			PlayerPrefs.SetFloat("TimeGame", tiempos[2]);
			PlayerPrefs.SetString("DificultadNivel", "facil");

			levelCurrent = Levels.facil;
			break;
				default:
						break;
		}

		if(Application.loadedLevelName != "memorygame_c#")
			Application.LoadLevel(1);
		else
		{

			rootHUD = "UI Root/Camera/HUD/";
			// Referencia al objeto "HUD" en la jerarquia
			hudMainParent = GameObject.Find (rootHUD);

			GameObject mini;
			mini = hudMainParent.transform.FindChild("Mini juego Inicio").gameObject;

			Destroy(mini);
			mini = null;

			Application.LoadLevel(1);
			GameObject bajararGO = GameObject.Find("Barajar");
			Bajarar barajaerScript = bajararGO.GetComponent<Bajarar>();

			barajaerScript.Iniciar();


		}


	}
	
	// Update is called once per frame
	void Update ()
	{	
		PlayerPrefs.SetString("ModeGame", levelCurrent.ToString());
//		Debug.LogError("Este es el tiempo dado: " + PlayerPrefs.GetFloat("TimeGame"));
	}

	public void SelectTime()
	{
		if(UIToggle.current.name == "TableHard" && UIToggle.current.value)
		{
			levelCurrent = Levels.dificil;


//			countDown.t = 60;
//			countDown.tiempoTotal = 60;
//			countDown.fill = 60;

			//buttonStart.isEnabled = true;

		}

		if(UIToggle.current.name == "TableNormal" && UIToggle.current.value)
		{
			levelCurrent = Levels.medio;

			PlayerPrefs.SetString("DificultadNivel", "medio");
//			countDown.t = 120;
//			countDown.tiempoTotal = 120;
//			countDown.fill = 120;

			//buttonStart.isEnabled = true;
//			PlayerPrefs.SetFloat("TimeGame", 120);
		}

		if(UIToggle.current.name == "TableEasy" && UIToggle.current.value)
		{
			levelCurrent = Levels.facil;

			PlayerPrefs.SetString("DificultadNivel", "facil");
//			countDown.t = 180;
//			countDown.tiempoTotal = 180;
//			countDown.fill = 180;

			//buttonStart.isEnabled = true;
//			PlayerPrefs.SetFloat("TimeGame", 180);
		}
	}
}
