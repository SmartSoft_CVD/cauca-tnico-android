﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class ChangeParts : MonoBehaviour {

	public GameObject [] listaBotones;
	public string spriteActual;

	public GameObject textsControllerObj;
	public TextsController textsController;



	void Awake()
	{
		textsControllerObj = GameObject.Find("TextsController");
		textsController = textsControllerObj.GetComponent<TextsController>();


	}
	// Use this for initialization
	void Start ()
	{


		listaBotones = GameObject.FindGameObjectsWithTag("Icon");

		for (int i = 0; i < listaBotones.Length; i++)
		{
//			Debug.LogWarning("Estos son los objetos con el tag Icon " + listaBotones[i]);
		}

		foreach (GameObject botonActual in listaBotones)
		{
			UIEventListener.Get(botonActual.transform.parent.gameObject).onClick += MostrarSprite;
			
		}
	}

	public void EstablecerTexto(int parteCambiada, string region, int indice)
	{
		switch (region)
		{
			case "Campesino":

				PlayerPrefs.SetString("textoActual", textsController.textosCampesinos[parteCambiada][indice]);

				break;

		case "Afro":
			
			PlayerPrefs.SetString("textoActual", textsController.textosAfro[parteCambiada][indice]);
			
			break;

		case "Guambiano":

			Debug.LogError("Elemento a cambiar: " + parteCambiada + " Indice del arreglo: " + indice);
			PlayerPrefs.SetString("textoActual", textsController.textosGuambianos[parteCambiada][indice]);
			
			break;

		case "Nasa":
			
			PlayerPrefs.SetString("textoActual", textsController.textosNasa[parteCambiada][indice]);
			
			break;

		case "Yanaconas":
			
			PlayerPrefs.SetString("textoActual", textsController.textosYanaconas[parteCambiada][indice]);
			
			break;
		default:
						break;
		}

			
	}
	void MostrarSprite(GameObject gO)
	{
//		Debug.LogWarning(gO.transform.FindChild ("Icon").GetComponent<UISprite>().spriteName);

		string nameComplete;
		string [] partsOfName;
		
		nameComplete = gO.transform.FindChild ("Icon").GetComponent<UISprite>().spriteName;
		partsOfName = nameComplete.Split(new char[]{ '_' });

//		Debug.LogWarning(partsOfName[0]);
		switch (partsOfName[0])
		{
		case "Cara":
				PlayerPrefs.SetString("Cara", gO.transform.FindChild ("Icon").GetComponent<UISprite>().spriteName);

			PartsReceived.restricciones[0] = true;
			break;

		case "Cabeza":
				PlayerPrefs.SetString("Cabeza", gO.transform.FindChild ("Icon").GetComponent<UISprite>().spriteName);
			PartsReceived.restricciones[1] = true;
			break;

		case "Camisa":
				PlayerPrefs.SetString("Camisa", gO.transform.FindChild ("Icon").GetComponent<UISprite>().spriteName);
			EstablecerTexto(0, partsOfName[1], int.Parse(partsOfName[2]));
			PartsReceived.restricciones[2] = true;
				
			break;

		case "Pantalon":
				PlayerPrefs.SetString("Pantalon", gO.transform.FindChild ("Icon").GetComponent<UISprite>().spriteName);
			EstablecerTexto(1, partsOfName[1], int.Parse(partsOfName[2]));
			PartsReceived.restricciones[3] = true;
			break;

		case "Zapatos":
				PlayerPrefs.SetString("Zapatos", gO.transform.FindChild ("Icon").GetComponent<UISprite>().spriteName);
			EstablecerTexto(2, partsOfName[1], int.Parse(partsOfName[2]));
			PartsReceived.restricciones[4] = true;
			break;

		case "Accesorios":
				PlayerPrefs.SetString("Accesorios", gO.transform.FindChild ("Icon").GetComponent<UISprite>().spriteName);
			EstablecerTexto(3, partsOfName[1], int.Parse(partsOfName[2]));
			PartsReceived.restricciones[5] = true;
			break;

		case "Sombreros":
				PlayerPrefs.SetString("Sombreros", gO.transform.FindChild ("Icon").GetComponent<UISprite>().spriteName);
			EstablecerTexto(3, partsOfName[1], int.Parse(partsOfName[2]));
			PartsReceived.restricciones[6] = true;
			PartsReceived.restricciones[5] = true;
				break;
				default:
						break;
		}

	}
	
	// Update is called once per frame
	void Update ()
	{

	
	}
}
