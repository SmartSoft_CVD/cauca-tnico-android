﻿using UnityEngine;
using System.Collections;

public enum ePrefab
{
	/* Este enum almacenara los diferentes prefabs que conforman las pantallas del juego
	 * NOTA: un prefab puede pertenecer a una o varias pantallas */

	/* Para diferenciar las Pantallas de los Prefabs usaremos como notación:
	 * _PREFAB al finalizar el nombre de cada una de las constantes de este enum */
	
	NONE,

	// Pantalla "Logo"
	LOGO_PREFAB,

	// Pantalla "Tutorial"
	TUTORIAL_PREFAB,

	// Pantalla "Selección"
	SELECTION_PREFAB,

	// Pantalla "Escoger Avatar"
	ESCOGER_AVATAR_PREFAB,

	// Pantalla "Crear Avatar"
	CREAR_AVATAR_PREFAB,

	// Pantalla "Características"
	AVATAR_PREFAB,
	DESCRIPTION_PREFAB,

	// Pantalla Minijuego Inicio
	MINIJUEGO_INICIO_PREFAB,

	// Pantalla "Felicitaciones"
	CONGRATULATIONS_PREFAB,

	// Pantalla "Dar personalidad"
	GIVE_PERSONALITY_PREFAB,

	// Pantalla "Dar personalidad (Texto 1)"
	PERSONALITY_TEXT1_PREFAB,

	// Pantalla "Dar personalidad (Texto 2)"
	PERSONALITY_TEXT2_PREFAB,

	// Pantalla "Avatar terminado"
	FINISHED_AVATAR_PREFAB,

	// Pantalla "Compartir Avatar"
	SHARE_AVATAR_PREFAB,

	// Pantalla "Capturar Avatar"
	CAPTURE_AVATAR_PREFAB,
	
	/* En caso de que existan mas Prefabs en el juego se
	 * deberán ir agregando como parte de las constantes de este enum */
}
