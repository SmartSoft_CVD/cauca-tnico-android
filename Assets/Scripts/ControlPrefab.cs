﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControlPrefab : MonoBehaviour {

	string [] partsOfName;
	string nameComplete;

	public List<string> listaNombresTexto1;
	public List<string> listaNombresTexto2;
	int indexWords;
	UILabel textPrefab;



	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public string ModifyText(GameObject gO, GameObject prefab, GameObject objIns)
	{

		GameObject child = prefab.transform.GetChild(0).gameObject;
		textPrefab = child.GetComponent<UILabel>();


		nameComplete = gO.name;
		partsOfName = nameComplete.Split(new char[]{ '_', ' ', '-' , '(' });

		indexWords = int.Parse(partsOfName[1]);

//		Debug.Log("nombre: " + prefab.name);

		if(objIns.transform.parent.name == "WordsBase1")
		{
			textPrefab.text = listaNombresTexto1[indexWords];
		}else if(objIns.transform.parent.name == "WordsBase2")
		{
			textPrefab.text = listaNombresTexto2[indexWords];
		}

		return textPrefab.text;
//		Debug.Log("El texto: " + textPrefab.text);
	}
}
