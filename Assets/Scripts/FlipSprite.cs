﻿using UnityEngine;
using System.Collections;

public class FlipSprite : MonoBehaviour
{
	//Paneles Game Modes & Find an opponent
	public GameObject card_front;
	public GameObject card_back;
	
	//Banderas
	public bool turning_card_front = false;
	public bool turning_card_back = false;


	//-----Flip Games modes-------
	
	public void flipChallengeMode(){
		if(!turning_card_front){        
			card_back.GetComponent<TweenRotation>().PlayForward();
			turning_card_front = true;
		} else{
			turning_card_front = false;
		}
	}
	
	//-----Flip Find an opponent mode-------
	
	public void flipChallengeBackFace(){
		if(turning_card_back){
			card_front.GetComponent<TweenRotation>().PlayReverse();
			turning_card_back= false;
		} else{
			turning_card_back = true;
		}
	}
}
