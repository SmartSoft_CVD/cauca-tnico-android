﻿using UnityEngine;
using System.Collections;
using System.IO;

public class Test : MonoBehaviour {

	public UISprite spriteNGUI;
	private Texture textureObtained;
	string nameSprite;

	UIAtlas currentAtlas;
	Texture2D tex;
	UISpriteData data;

	public UITexture uiTexture;
	public Texture2D texture;



	// Use this for initialization
	void Start ()
	{
		currentAtlas = spriteNGUI.atlas;
		nameSprite = spriteNGUI.spriteName;
		
		data = currentAtlas.GetSprite(nameSprite);
	}

	private IEnumerator MetodoEduardo(UISpriteData _data)
	{
		Debug.Log("Atlas actual: " + currentAtlas + " data Actual: " + data.name + " Nombre del sprite: " + nameSprite);
		yield return new WaitForEndOfFrame ();		
		
		// Create a texture the size of the screen, RGB24 format
		int width = _data.width;
		int height = _data.height;

		Debug.Log("X: " + _data.x + " Y: " + _data.y + " Ancho: " + _data.width + " Alto: " + _data.height);

		tex = new Texture2D ( _data.width, _data.height, TextureFormat.RGB24, false );



		// Read screen contents into the texture
		tex.ReadPixels (new Rect(_data.x, _data.y, _data.width, _data.height), 0, 0 );
		tex.Apply ();

		Debug.Log(tex.name);

		// Encode texture into PNG
//		byte[] bytes = tex.EncodeToPNG ();
//
//		File.WriteAllBytes(Application.dataPath + "/Assets/" + "imagen.png", bytes);
	}

	// Update is called once per frame
	void Update ()
	{

	
	
	}

	void OnGUI()
	{
		if(GUI.Button(new Rect(Screen.width * 0.2f, Screen.height * 0.2f, Screen.height * 0.2f, Screen.height * 0.2f), "Botón tocar"))
		{
			StartCoroutine(MetodoEduardo(data));

		}

		GUI.DrawTexture(new Rect(0, 0, 100, 100), tex);

	}
}
