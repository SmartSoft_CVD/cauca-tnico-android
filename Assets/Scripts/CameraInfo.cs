﻿using UnityEngine;
using System.Collections;

public class CameraInfo : MonoBehaviour {

	public int alto;
	public int ancho;
	public int espacioNoUtilizado;

	public UISprite spriteEsapcio;
	public UISprite spriteEsapcio2;
	public UISprite spriteEsapcio3;
	public GameObject casilla;

	public int numeroCasillas;
	float tamanoCasillas;

	float desplazamientoX;
	float desplazamientoY;

	// Use this for initialization
	void Start ()
	{
		InstaciarCartas();
	
	}
	
	// Update is called once per frame
	void Update ()
	{

	}

	void InstaciarCartas()
	{
		alto = Screen.height;
		ancho = Screen.width;
		
		espacioNoUtilizado = (ancho - alto) / 2;
		spriteEsapcio.width = espacioNoUtilizado;
		
		
		Vector3 posicionEspacio2 = spriteEsapcio2.transform.localPosition;
		posicionEspacio2 = new Vector3(espacioNoUtilizado, 0, 0);
		spriteEsapcio2.transform.localPosition = posicionEspacio2;
		
		spriteEsapcio2.width = alto;
		
		Vector3 posicionEspacio3 = spriteEsapcio3.transform.localPosition;
		posicionEspacio3 = new Vector3(espacioNoUtilizado + alto, 0, 0);
		spriteEsapcio3.transform.localPosition = posicionEspacio3;
		
		spriteEsapcio3.width = espacioNoUtilizado;
		
		spriteEsapcio.height = alto;
		spriteEsapcio2.height = alto;
		spriteEsapcio3.height = alto;
		
		tamanoCasillas = alto / numeroCasillas;

		desplazamientoY = 0;
		
		for (int d = 0; d < numeroCasillas; d++)
		{
			desplazamientoX = 0;
			
			for (int i = 0; i < numeroCasillas; i++)
			{
				GameObject cartaInstanciada = GameObject.Instantiate(casilla, Vector3.zero, Quaternion.identity) as GameObject;
				
				
				cartaInstanciada.transform.parent = spriteEsapcio2.transform;
				
				cartaInstanciada.transform.localPosition = new Vector3(desplazamientoX, desplazamientoY, 0);
				cartaInstanciada.transform.localScale = Vector3.one;
				
				cartaInstanciada.GetComponent<UISprite>().width = (int)tamanoCasillas;
				cartaInstanciada.GetComponent<UISprite>().height = (int)tamanoCasillas;
				
				desplazamientoX += tamanoCasillas;
				
			}
			desplazamientoY -= tamanoCasillas;
		}
	}
}
