﻿using UnityEngine;
using System.Collections;

public class DisableTween : MonoBehaviour {

	public TweenPosition tweenPos;
	// Update is called once per frame
	public void DisabledTween ()
	{
		tweenPos.enabled = false;
	}
}
