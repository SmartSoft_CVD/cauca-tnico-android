﻿//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2014 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;

public enum Propiedades { PROPIEDAD1, PROPIEDAD2, PROPIEDAD3 }
[AddComponentMenu("NGUI/Examples/Drag and Drop Item (Example)")]
public class PlaceCorrect : UIDragDropItem
{
	public GameObject prefab;
	public GameObject parent;
	
	public Propiedades propiedadActual;
	
	RaycastHit hit;
	Ray ray;
	
	UILabel labelCurrent;
	string textCurrent;
	public static GameObject objIns;

	public bool posicionarPieza = false;

	protected override void OnDragDropMove (Vector3 delta)
	{	
		Debug.Log("Se esta moviendo");


		base.OnDragDropMove(delta);
	}
	
	protected override void OnDragDropRelease (GameObject surface)
	{
		hit = UICamera.lastHit;
		ray = UICamera.currentRay;		
		
		if (Physics.Raycast(ray, out hit, 100.0f))
		{

			if(hit.collider.name == this.gameObject.name)
			{
				posicionarPieza = true;
				if (mGrid != null) mGrid.repositionNow = true;

				mParent = hit.transform;
				float colorPiece = mTrans.GetComponent<UISprite>().color.a;
				colorPiece = 255f;
				mTrans.GetComponent<UISprite>().color = new Color(mTrans.GetComponent<UISprite>().color.r,
				                                                  mTrans.GetComponent<UISprite>().color.g,
				                                                  mTrans.GetComponent<UISprite>().color.b, colorPiece);


			}
			else
				posicionarPieza = false;
		}
		base.OnDragDropRelease(surface);
	}



	void Update()
	{
		if(posicionarPieza)
		{
			this.gameObject.transform.position = Vector3.Lerp(this.gameObject.transform.position, hit.collider.gameObject.transform.position, 20.0f * Time.deltaTime);

			if(this.gameObject.transform.position == hit.collider.gameObject.transform.position)
			{
				Destroy(this.gameObject.GetComponent<PlaceCorrect>());
//				this.gameObject.GetComponent<PlaceCorrect>().enabled = false;
			}
		}
		else
			Debug.Log("La pieza esta mal ubicada");
	}

}


