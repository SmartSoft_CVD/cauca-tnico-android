﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bajarar : MonoBehaviour {
	
	public GameObject memorycard;

	public GameObject [] cardsPrefabs;
	public List<string> cards;	
	public List<Card> cardslist = new List<Card>();
	public List<int> possibleCards = new List<int>();

	private GameObject lJObj;
	private LogicaJuego lJScript;

	public GameObject padre;

	public GameObject buttonStart;

	int index;
	string difficulty;

	public int nivel = 1;

	public float [] posicionesXCartas;
	public float [] posicionesYCartas;

	public class Card{
		public int number;
		public string texture;
		
		public Card(int n, string t){
			number = n;
			texture = t;
		}
	}

	public int alto;
	public int ancho;
	public int espacioNoUtilizado;
	
	public UISprite spriteEsapcio;
	public UISprite spriteEsapcio2;
	public UISprite spriteEsapcio3;
	public GameObject casilla;
	
	public int numeroCasillas;
	private float tamanoCasillas;

	public float TamanoCasillas {
		get {
			return tamanoCasillas;
		}
		set {
			tamanoCasillas = value;
		}
	}
	
	float desplazamientoX;
	float desplazamientoY;

	public List<Transform> numeroDeCartas = new List<Transform>();

	public int[] vectorNivelesFacil;
	public int[] vectorNivelesMedio;
	public int[] vectorNivelesDificil;

	public int[] vectorNiveles;

	//new int[2, 4, 4, 6, 6, 6]
	int posicionArreglo = 0;

	public int segundos;
	public bool executePowerUp = false;
	public bool executePowerUpFlip = false;

	public CountDown countDown;
	public bool executeMethod = false;

	bool oneTime = false;
	bool oneTimeComprobar = false;

	// Use this for initialization
	void Start ()
	{
		PlayerPrefs.SetString("Dificultad", PlayerPrefs.GetString("ModeGame"));

		difficulty = PlayerPrefs.GetString("DificultadNivel");

		Debug.LogError("<------------------------- Esta es la dificultad: " + PlayerPrefs.GetString("DificultadNivel") + " ------------------------------------>");


		switch (difficulty)
		{
			case "facil":

			vectorNiveles[0] = 2;
			vectorNiveles[1] = 2;
			vectorNiveles[2] = 4;
			vectorNiveles[3] = 4;
			vectorNiveles[4] = 4;
			vectorNiveles[5] = 4;
			vectorNiveles[6] = 6;

			Debug.Log("<------------------------------------------------------- Index de nivel facil: " + index);
			Debug.Log("El nivel es facil");
			
				break;
			case "medio":

			vectorNiveles[0] = 2;
			vectorNiveles[1] = 4;
			vectorNiveles[2] = 4;
			vectorNiveles[3] = 4;
			vectorNiveles[4] = 4;
			vectorNiveles[5] = 6;
			vectorNiveles[6] = 6;
				
			Debug.Log("<------------------------------------------------------- Index de nivel medio: " + index);
			Debug.Log("El nivel es medio");
				break;
			case "dificil":

			vectorNiveles[0] = 4;
			vectorNiveles[1] = 4;
			vectorNiveles[2] = 4;
			vectorNiveles[3] = 6;
			vectorNiveles[4] = 6;
			vectorNiveles[5] = 6;
			vectorNiveles[6] = 6;
				
			Debug.Log("<------------------------------------------------------- Index de nivel dificil: " + index);
			Debug.Log("El nivel es dificil");
				break;
				default:
						break;
		}

//		Debug.LogWarning("Numero de posibles cartas: " + possibleCards.Count);
		executeMethod = false;

		lJObj = this.gameObject;
		lJScript = lJObj.GetComponent<LogicaJuego>();

		//Debe iniciar cuando se presiona el botón

		Invoke("Iniciar", 0.5f);

	//	EventDelegate.Add(buttonStart.transform.parent.GetComponent<TweenPosition>().onFinished, Iniciar);

		EventDelegate.Add(buttonStart.transform.parent.GetComponent<TweenPosition>().onFinished, delegate
		                  { 
			executeMethod = true;
//			Debug.LogError("-----------------------------------------------------------> executeMethod: " + executeMethod);


		});



		InvokeRepeating("Evaluar", 0f, segundos);

		alto = Screen.height;
		ancho = Screen.width;
		
		espacioNoUtilizado = (ancho - alto) / 2;
		spriteEsapcio.width = espacioNoUtilizado;
		
		
		Vector3 posicionEspacio2 = spriteEsapcio2.transform.localPosition;
		posicionEspacio2 = new Vector3(espacioNoUtilizado, 0, 0);
		spriteEsapcio2.transform.localPosition = posicionEspacio2;
		
		spriteEsapcio2.width = alto;
		
		Vector3 posicionEspacio3 = spriteEsapcio3.transform.localPosition;
		posicionEspacio3 = new Vector3(espacioNoUtilizado + alto, 0, 0);
		spriteEsapcio3.transform.localPosition = posicionEspacio3;
		
		spriteEsapcio3.width = espacioNoUtilizado;
		
		spriteEsapcio.height = alto;
		spriteEsapcio2.height = alto;
		spriteEsapcio3.height = alto;
	}

	void PowerUp()
	{
//		Debug.LogWarning("Se ejecuto el powerup");

	}

	void Evaluar()
	{

		int numeroAleatorio = Random.Range(1, 10);

		Debug.LogError("Numero aleatorio: " + numeroAleatorio.ToString());

		if(executeMethod)
		{
			if(numeroDeCartas.Count != 4)
			{
				// TODO if(!executePowerUp)
				// TODO {

					#region Probabilidad dependiendo de los niveles de matriz de 6X6



					// ---------------------------------------------------------------


					#endregion

					if(countDown.t < PlayerPrefs.GetFloat("TimeGame") / 3)
					{


						if(numeroAleatorio >= 1 && numeroAleatorio <= 2)
						{
							if(nivel >= 2)
							{
								if(!executePowerUp)
								// Invocar metodo de la clase logica juego
								lJScript.RandomPowerUp();
							}

							
						} if(numeroAleatorio >= 8 && numeroAleatorio <= 9)
						{
							if(nivel >= 4)
							{
								if(nivel == 4 || nivel == 5 || nivel == 6)
								{
									if(!executePowerUp)
									// Invocar metodo de la clase logica juego
									lJScript.RandomPowerUp();
								}
							}
							//Ejecutar metodo power up
							
							
							
						}
					}




					if(countDown.t < PlayerPrefs.GetFloat("TimeGame") / 2)
					{
						if(numeroAleatorio >= 1 && numeroAleatorio <= 2)
						{
							if(nivel >= 2)
							{
								if(!executePowerUp)
								// Invocar metodo de la clase logica juego
								lJScript.RandomPowerUp();
							}
							//Ejecutar metodo power up
	//						executePowerUp = true;


						}
					}

					if(countDown.t < PlayerPrefs.GetFloat("TimeGame"))
					{
						if(numeroAleatorio == 1)
						{
							if(nivel >= 2)
							{
								if(!executePowerUp)
								// Invocar metodo de la clase logica juego
								lJScript.RandomPowerUp();
							}
							//Ejecutar metodo power up
	//						executePowerUp = true;


						}
					}
					// TODO }
			}


			if(numeroDeCartas.Count != 4)
			{
				#region Region para el power up de vltear cartas
				// TODO if(!executePowerUpFlip)
				// TODO {
	//				if(numeroAleatorio >= 6 && numeroAleatorio <= 10)
	//				{
	//					if(nivel >= 2)
	//					{
	//						if(nivel == 4 || nivel == 5 || nivel == 6)
	//						{
	//							// Invocar metodo de la clase logica juego
	//							lJScript.RandomPowerUpFlip();
	//						}
	//					}
	//					//Ejecutar metodo power up
	//					executePowerUpFlip = true;
	//					
	//					
	//				}
					
					if(countDown.t < PlayerPrefs.GetFloat("TimeGame") / 3)
					{

						if(numeroAleatorio >= 4 && numeroAleatorio <= 5)
						{
							if(nivel >= 2)
							{
								//Ejecutar metodo power up
	//							executePowerUpFlip = true;
								// Invocar metodo de la clase logica juegoç
								if(!executePowerUpFlip)
								lJScript.RandomPowerUpFlip();

							}

							
							
						}
						 if(numeroAleatorio >= 8 && numeroAleatorio <= 10)
						{
							
							Debug.LogWarning("<-------------------------- el numero aleatorio esta entre 6 y 10 --------------------------------------->");
							if(nivel >= 4)
							{
								Debug.LogWarning("<-------------------------- el nivel es mayor a 2 --------------------------------------->");
								if(nivel == 4 || nivel == 5 || nivel == 6)
								{
									Debug.LogWarning("<-------------------------- el nivel es o 4 o 5 o 6 --------------------------------------->");
									// Invocar metodo de la clase logica juego
									if(!executePowerUpFlip)
									lJScript.RandomPowerUpFlip();


									Debug.LogWarning("<-------------------------- se lanza el metodo de voltear cartas --------------------------------------->");
								}
							}
							
							
							//					executePowerUpFlip = true;
							
							
						}
					}


					
					if(countDown.t < PlayerPrefs.GetFloat("TimeGame") / 2)
					{
						if(numeroAleatorio >= 4 && numeroAleatorio <= 5)
						{
							if(nivel >= 2)
							{
								// Invocar metodo de la clase logica juego
								if(!executePowerUpFlip)
								lJScript.RandomPowerUpFlip();
							}
							//Ejecutar metodo power up
	//						executePowerUpFlip = true;
							
							
						}
					}
					
					if(countDown.t < PlayerPrefs.GetFloat("TimeGame"))
					{
						if(numeroAleatorio == 2)
						{
							if(nivel >= 2)
							{
								// Invocar metodo de la clase logica juego
								if(!executePowerUpFlip)
								lJScript.RandomPowerUpFlip();
							}
							//Ejecutar metodo power up
	//						executePowerUpFlip = true;
							
							
						}
					}
				// TODO }
			#endregion
			}
		}

	}

	public void Iniciar()
	{
		Debug.Log("-----------------------------");
		nivel++;
	 	oneTime = false;
		oneTimeComprobar = false;


		desplazamientoX = 0;
		desplazamientoY = -alto * 0.01f;
		tamanoCasillas = 0;

		lJScript.nroftries = 0;
		cards.Clear();



//		PlayerPrefs.SetString("Dificultad", PlayerPrefs.GetString("ModeGame"));
//
//		difficulty = PlayerPrefs.GetString("ModeGame");
//
//		switch (difficulty)
//		{
//			case "facil":
//
//			vectorNiveles[0] = 2;
//			vectorNiveles[1] = 2;
//			vectorNiveles[2] = 4;
//			vectorNiveles[3] = 4;
//			vectorNiveles[4] = 4;
//			vectorNiveles[5] = 4;
//			vectorNiveles[6] = 6;
//
//			Debug.Log("<------------------------------------------------------- Index de nivel facil: " + index);
//			Debug.Log("El nivel es facil");
//			
//				break;
//			case "medio":
//
//			vectorNiveles[0] = 2;
//			vectorNiveles[1] = 4;
//			vectorNiveles[2] = 4;
//			vectorNiveles[3] = 4;
//			vectorNiveles[4] = 4;
//			vectorNiveles[5] = 6;
//			vectorNiveles[6] = 6;
//				
//			Debug.Log("<------------------------------------------------------- Index de nivel medio: " + index);
//			Debug.Log("El nivel es medio");
//				break;
//			case "dificil":
//
//			vectorNiveles[0] = 4;
//			vectorNiveles[1] = 4;
//			vectorNiveles[2] = 4;
//			vectorNiveles[3] = 6;
//			vectorNiveles[4] = 6;
//			vectorNiveles[5] = 6;
//			vectorNiveles[6] = 6;
//				
//			Debug.Log("<------------------------------------------------------- Index de nivel dificil: " + index);
//			Debug.Log("El nivel es dificil");
//				break;
//				default:
//						break;
//		}


		
	
		index = vectorNiveles[posicionArreglo];
		
		if(posicionArreglo == vectorNiveles.Length-1)
		{
			posicionArreglo = -1;
			
		}
		
		posicionArreglo++;





//		Debug.Log("pos arreglo: " + posicionArreglo);

//		cards.Count = index;

		// Cada nuevo juego se aumenta el número de cartas
		//cards.Insert(cards.Count, (index).ToString());
		for (int w = 0; w < index * index /2; w++)
		{
			cards.Insert(w, (w + 1).ToString());

		}


		for(int i=0; i<(index*index)/2; i++){

//			Debug.Log ("Valor i: " + i);
			//Debug.Log ("Valor de card en la pos i: " + cards[i]);
			cardslist.Add(new Card(i, cards[i]));
			//cardslist.Add(new Card(i, cards[i]));
			possibleCards.Add(i);
			possibleCards.Add(i);
		}

		Debug.LogError("<------------------------------------------------------------------------------------------->");
//		Debug.Log ("Valor de cardList: " + cardslist.Count);
		
		if(cardslist.Count > 0)
			ShuffleCards();
	}


	public IEnumerator PowerUpDestapar()
	{
		yield return new WaitForSeconds(0.5f);
		foreach (Transform cartaAMostrar in numeroDeCartas)
		{
			if(cartaAMostrar != null)
			{
				//cartaAMostrar.gameObject.GetComponent<UIButton>().isEnabled = false;
				cartaAMostrar.gameObject.GetComponent<IntanciacionTarjetas>().DestaparCartas();
//				yield return new WaitForSeconds(0.01f);
			}
		}
	}


	IEnumerator MostrarCartas()
	{
		yield return new WaitForSeconds(0.5f);
		oneTime = false;

			if(!oneTime)
			{
				foreach (Transform cartaAMostrar in numeroDeCartas)
				{
					if(cartaAMostrar != null)
					{
						//TODO Aquí desactivo el colosionador de las cartas recien se instancian TODO
						cartaAMostrar.gameObject.GetComponent<UIButton>().isEnabled = false;
						cartaAMostrar.gameObject.GetComponent<IntanciacionTarjetas>().MCM();
						yield return new WaitForSeconds(0.01f);
//						yield return new WaitForEndOfFrame();

					StartCoroutine(ComprobarCartas());
						
					}
				}
			}

	}

	IEnumerator metodoOcultarCartas()
	{

		yield return new WaitForSeconds(1.0f);

		oneTime = true;
		if(oneTime)
		{
			foreach (Transform cartaAMostrar in numeroDeCartas)
			{
				if(cartaAMostrar != null)
				{
					cartaAMostrar.gameObject.GetComponent<IntanciacionTarjetas>().Hide();
//					yield return new WaitForSeconds(0.2f);
					yield return new WaitForEndOfFrame();
//					yield return new WaitForEndOfFrame();

				}

			}
		}


	}

	IEnumerator ComprobarCartas()
	{
		yield return new WaitForEndOfFrame();
		oneTimeComprobar = true;

		yield return new WaitForSeconds(1.6f);
		if(oneTimeComprobar)
		{
			foreach (Transform cartaAMostrar in numeroDeCartas)
			{
				if(cartaAMostrar != null)
				{

					cartaAMostrar.gameObject.GetComponent<IntanciacionTarjetas>().comprobarCarta = true;

					//TODO Aquí activo de nuevo el colisionador de las cartas para que sean presionadas TODO

					cartaAMostrar.gameObject.GetComponent<UIButton>().isEnabled = true;
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		//difficulty = PlayerPrefs.GetString("ModeGame");
		Debug.Log("Nivel actual: " + nivel);

		PlayerPrefs.SetInt("NivelParejas", posicionArreglo);

		if(Input.GetKeyDown(KeyCode.F))
		{
			StartCoroutine(PowerUpDestapar());
		}

		for (int d = 0; d < cardslist.Count; d++)
		{
//			Debug.Log(cardslist[d]);
		}

//		Debug.Log("Este es el nivel: " + nivel);
	}
	
	void ShuffleCards(){
		int nrofcards = /*cardslist.Count*/index;


		lJScript.SetSetsOfCards(cards.Count);
		List<Card> temp = new List<Card>();
		
//		for(int i =0; i<nrofcards; i++){
//			int random = Random.Range(0, nrofcards-i);
//			temp.Add(cardslist[random]);
//			cardslist.RemoveAt(random);
//		}

		for (int s = 0; s < cardslist.Count; s++)
		{
//			Debug.Log(cardslist[s].number);

				}
		
//		cardslist = temp;

//		Debug.Log("aaaaaaaaaaaaaaaaaa: " + cardslist.Count);
//		for (int s = 0; s < cardslist.Count; s++)
//		{
//			print("gggggg: " + cardslist[s].number);
//		}
		SpawnCards();
	}
	
	void SpawnCards(){






//		Debug.Log("Esta es la cantidad de cartas: " + cardslist.Count);
//		for(int i=0; i<cardslist.Count; i++){
//			GameObject mc = Instantiate(memorycard) as GameObject;
//			mc.transform.parent = padre.transform;
//			mc.transform.localScale = Vector3.one;
//
//			mc.transform.localPosition = new Vector3((i%cardsinrow+(i%cardsinrow*spacebetweencards))-(cardsinrow/2f)+spacebetweencards, (i/cardsinrow+(i/cardsinrow*spacebetweencards))-(cardsincolumn/2f)+spacebetweencards, 0);
//			mc.GetComponentInChildren<IntanciacionTarjetas>().SetMemorycard(cardslist[i].texture, cardslist[i].number);
//		}

//		Debug.Log("Este es el numero de casillas: " + cardslist.Count);
//		numeroCasillas = cardslist.Count;
		numeroCasillas = index;
		tamanoCasillas = (alto-(alto * 0.02f)) / numeroCasillas ;

		for (int d = 0; d < /*cardslist.Count / 2*/ index; d++)
		{
			desplazamientoX = alto * 0.01f;
			
			for (int i = 0; i < /*cardslist.Count / 2*/index; i++)
			{
				GameObject cartaInstanciada = GameObject.Instantiate(memorycard, Vector3.zero, Quaternion.identity) as GameObject;


				cartaInstanciada.transform.parent = spriteEsapcio2.transform;
				cartaInstanciada.transform.localScale = Vector3.one;

				
				cartaInstanciada.transform.localPosition = new Vector3(desplazamientoX + tamanoCasillas/2 ,
				                                                       desplazamientoY - tamanoCasillas/2 ,
				                                                       0);


//				print("Index Primero: " + index);

//				for (int e = 0; e < index / 2; e++)
//				{
//					print("Index: " + e);
				int cartica = Random.Range(0,possibleCards.Count);
				int cc = possibleCards[cartica];
				possibleCards.RemoveAt(cartica);
					cartaInstanciada.GetComponentInChildren<IntanciacionTarjetas>().SetMemorycard(cardslist[cc].texture, cardslist[cc].number);
//				}


//				cartaInstanciada.GetComponent<UISprite>().width = (int)tamanoCasillas;
//				cartaInstanciada.GetComponent<UISprite>().height = (int)tamanoCasillas;

				Vector3 boxColliderCarta = cartaInstanciada.GetComponentInChildren<BoxCollider>().size;
				boxColliderCarta = new Vector3(tamanoCasillas - (int)(alto * 0.01f), tamanoCasillas  - (int)(alto * 0.01f), 0);
				cartaInstanciada.GetComponentInChildren<BoxCollider>().size = boxColliderCarta;

//				Vector3 boxColliderCartaCentro = cartaInstanciada.GetComponentInChildren<BoxCollider>().center;
//				boxColliderCartaCentro = new Vector3(cartaInstanciada.GetComponentInChildren<BoxCollider>().size.x / 2, cartaInstanciada.GetComponentInChildren<BoxCollider>().size.y / 2, 0);
//				cartaInstanciada.GetComponentInChildren<BoxCollider>().center = boxColliderCarta;

				Vector3 tamanoEstaCarta = cartaInstanciada.transform.FindChild("F").transform.localScale;
				tamanoEstaCarta = new Vector3(((int)tamanoCasillas - (int)(alto * 0.01f)) / 2, ((int)tamanoCasillas - (int)(alto * 0.01f)) / 2, 0);
				cartaInstanciada.transform.FindChild("F").transform.localScale = tamanoEstaCarta;

				Vector3 tamanoBackCarta = cartaInstanciada.transform.FindChild("B").transform.localScale;
				tamanoBackCarta = new Vector3((int)tamanoCasillas / 2, (int)tamanoCasillas / 2, 0);
				cartaInstanciada.transform.FindChild("B").transform.localScale = tamanoBackCarta;


//				cartaInstanciada.transform.FindChild("Front").GetComponent<UISprite>().width = (int)tamanoCasillas - (int)(alto * 0.01f);
//				cartaInstanciada.transform.FindChild("Front").GetComponent<UISprite>().height = (int)tamanoCasillas - (int)(alto * 0.01f);
//
//				cartaInstanciada.transform.FindChild("Back").GetComponent<UISprite>().width = (int)tamanoCasillas;
//				cartaInstanciada.transform.FindChild("Back").GetComponent<UISprite>().height = (int)tamanoCasillas;

				cartaInstanciada.GetComponentInChildren<IntanciacionTarjetas>().comprobarCarta = false;
				cartaInstanciada.GetComponent<UIButton>().isEnabled = false;
//				cartaInstanciada.transform.GetChild(0).transform.FindChild("Front").GetComponent<UISprite>().width = (int)tamanoCasillas;
//				cartaInstanciada.transform.GetChild(0).transform.FindChild("Front").GetComponent<UISprite>().height = (int)tamanoCasillas;
//				
//				cartaInstanciada.transform.GetChild(0).transform.FindChild("Back").GetComponent<UISprite>().width = (int)tamanoCasillas;
//				cartaInstanciada.transform.GetChild(0).transform.FindChild("Back").GetComponent<UISprite>().height = (int)tamanoCasillas;

				
				desplazamientoX += tamanoCasillas /*+ (alto * 0.01f)*/;

//				cartaInstanciada.GetComponentInChildren<UISprite>().width / 2
				
			}
			desplazamientoY -= tamanoCasillas;
		}

		foreach (Transform ss in spriteEsapcio2.transform)
		{
			numeroDeCartas.Add(ss);

			
		}



		foreach (Transform childCurrent in this.transform)
		{
			lJScript.cardsInstantiated.Add(childCurrent.gameObject);
		}

		if(numeroDeCartas.Count != 0)
		{


		}

		StartCoroutine(MostrarCartas());
	
	}


}
