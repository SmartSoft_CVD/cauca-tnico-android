﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AvatarsControllerChoose : MonoBehaviour
{

	
		public GameObject[] arrows;
		public UICenterOnChild grid;
		public List <GameObject> itemsOfGrid;
		GameObject objCentered;
		private string regionCurrent;
		private GameObject gridCurrent;
		public GameObject parentGrids;

		public List <GameObject> points;

	public GameObject pointsCurrent;
		public GameObject containerPoints;

	public GameObject prefabPoint;

	public UIButton buttonPlayMore;

	private int indiceBotonJugarMas;

	public UIButton buttonRight;
	public UIButton buttonLeft;


		void Awake ()
		{


				regionCurrent = PlayerPrefs.GetString ("RegionActual");

		for (int g = 0; g < itemsOfGrid.Count; g++)
		{
			if (itemsOfGrid [g] != null) {
				
				// Desactivamos la pantalla actual
				itemsOfGrid [g].SetActive (false); // (opcional)
				
				// Eliminamos HUD padre
				GameObject.Destroy (itemsOfGrid [g]);
				itemsOfGrid [g] = null;
			}
		}

//		if (itemsOfGrid [0] != null) {
//			
//			// Desactivamos la pantalla actual
//			itemsOfGrid [0].SetActive (false); // (opcional)
//			
//			// Eliminamos HUD padre
//			GameObject.Destroy (itemsOfGrid [0]);
//			itemsOfGrid [0] = null;
//		}
//		
//		if (itemsOfGrid [1] != null) {
//			
//			// Desactivamos la pantalla actual
//			itemsOfGrid [1].SetActive (false); // (opcional)
//			
//			// Eliminamos HUD padre
//			GameObject.Destroy (itemsOfGrid [1]);
//			itemsOfGrid [1] = null;
//		}
//		
//		if (itemsOfGrid [2] != null) {
//			
//			// Desactivamos la pantalla actual
//			itemsOfGrid [2].SetActive (false); // (opcional)
//			
//			// Eliminamos HUD padre
//			GameObject.Destroy (itemsOfGrid [2]);
//			itemsOfGrid [2] = null;
//		}
//		
//		if (itemsOfGrid [3] != null) {
//			
//			// Desactivamos la pantalla actual
//			itemsOfGrid [3].SetActive (false); // (opcional)
//			
//			// Eliminamos HUD padre
//			GameObject.Destroy (itemsOfGrid [3]);
//			itemsOfGrid [3] = null;
//		}

		if (pointsCurrent != null) {
			
			// Desactivamos la pantalla actual
			pointsCurrent.SetActive (false); // (opcional)
			
			// Eliminamos HUD padre
			GameObject.Destroy (pointsCurrent);
			pointsCurrent = null;
		}




		}
	
		// Use this for initialization
		void Start ()
		{

		EventDelegate.Add(buttonRight.onClick, delegate() {


			itemsOfGrid[1].SendMessage("OnClick");


			AdvanceByButtonsRight();

		});

		EventDelegate.Add(buttonLeft.onClick, delegate() {
			
			AdvanceByButtonsLeft();
		});

		points.Sort();
			
			switch (regionCurrent) {
			case "Campesinos":
				
				//Caras
				itemsOfGrid [0] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Campesino_1"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				itemsOfGrid [0].name = "Avatar Campesino 1";			
				// Lo agregamos como hijo del HUD
				itemsOfGrid [0].transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				itemsOfGrid [0].transform.localPosition = Vector3.zero;
				itemsOfGrid [0].transform.localRotation = Quaternion.identity;
				itemsOfGrid [0].transform.localScale = Vector3.one;
				
				//Caras
				itemsOfGrid [1] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Campesino_2"));			
				// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
				itemsOfGrid [1].name = "Avatar Campesino 2";			
				// Lo agregamos como hijo del HUD
				itemsOfGrid [1].transform.parent = parentGrids.transform;		
				// La asignaciom modifica el Transform, asi que lo reiniciamos
				itemsOfGrid [1].transform.localPosition = Vector3.zero;
				itemsOfGrid [1].transform.localRotation = Quaternion.identity;
				itemsOfGrid [1].transform.localScale = Vector3.one;

			itemsOfGrid [2] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Campesino_3"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [2].name = "Avatar Campesino 3";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [2].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [2].transform.localPosition = Vector3.zero;
			itemsOfGrid [2].transform.localRotation = Quaternion.identity;
			itemsOfGrid [2].transform.localScale = Vector3.one;

			itemsOfGrid [3] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Campesino_4"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [3].name = "Avatar Campesino 4";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [3].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [3].transform.localPosition = Vector3.zero;
			itemsOfGrid [3].transform.localRotation = Quaternion.identity;
			itemsOfGrid [3].transform.localScale = Vector3.one;

			itemsOfGrid [4] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Campesino_5"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [4].name = "Avatar Campesino 5";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [4].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [4].transform.localPosition = Vector3.zero;
			itemsOfGrid [4].transform.localRotation = Quaternion.identity;
			itemsOfGrid [4].transform.localScale = Vector3.one;

			itemsOfGrid [5] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Campesino_6"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [5].name = "Avatar Campesino 6";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [5].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [5].transform.localPosition = Vector3.zero;
			itemsOfGrid [5].transform.localRotation = Quaternion.identity;
			itemsOfGrid [5].transform.localScale = Vector3.one;

			#region
			pointsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/BasesPoints/BasePointCampesinos"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			pointsCurrent.name = "Point Currents";			
			// Lo agregamos como hijo del HUD
			pointsCurrent.transform.parent = this.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			pointsCurrent.transform.localPosition = new Vector3(0, -482, 0);
			pointsCurrent.transform.localRotation = Quaternion.identity;
			pointsCurrent.transform.localScale = Vector3.one;
			#endregion

			indiceBotonJugarMas = 5;
				
				break;

		case "Afro":


			//Caras
			itemsOfGrid [0] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Afro_1"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [0].name = "Avatar Afro 1";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [0].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [0].transform.localPosition = Vector3.zero;
			itemsOfGrid [0].transform.localRotation = Quaternion.identity;
			itemsOfGrid [0].transform.localScale = Vector3.one;
			
			//Caras
			itemsOfGrid [1] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Afro_2"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [1].name = "Avatar Afro 2";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [1].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [1].transform.localPosition = Vector3.zero;
			itemsOfGrid [1].transform.localRotation = Quaternion.identity;
			itemsOfGrid [1].transform.localScale = Vector3.one;

			itemsOfGrid [2] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Afro_3"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [2].name = "Avatar Afro 3";				
			// Lo agregamos como hijo del HUD
			itemsOfGrid [2].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [2].transform.localPosition = Vector3.zero;
			itemsOfGrid [2].transform.localRotation = Quaternion.identity;
			itemsOfGrid [2].transform.localScale = Vector3.one;

			itemsOfGrid [3] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Afro_4"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [3].name = "Avatar Afro 4";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [3].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [3].transform.localPosition = Vector3.zero;
			itemsOfGrid [3].transform.localRotation = Quaternion.identity;
			itemsOfGrid [3].transform.localScale = Vector3.one;

			itemsOfGrid [4] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Afro_5"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [4].name = "Avatar Afro 5";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [4].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [4].transform.localPosition = Vector3.zero;
			itemsOfGrid [4].transform.localRotation = Quaternion.identity;
			itemsOfGrid [4].transform.localScale = Vector3.one;

			itemsOfGrid [5] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Afro_6"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [5].name = "Avatar Afro 6";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [5].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [5].transform.localPosition = Vector3.zero;
			itemsOfGrid [5].transform.localRotation = Quaternion.identity;
			itemsOfGrid [5].transform.localScale = Vector3.one;

			itemsOfGrid [6] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Afro_7"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [6].name = "Avatar Afro 7";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [6].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [6].transform.localPosition = Vector3.zero;
			itemsOfGrid [6].transform.localRotation = Quaternion.identity;
			itemsOfGrid [6].transform.localScale = Vector3.one;

			itemsOfGrid [7] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Afro_8"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [7].name = "Avatar Afro 8";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [7].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [7].transform.localPosition = Vector3.zero;
			itemsOfGrid [7].transform.localRotation = Quaternion.identity;
			itemsOfGrid [7].transform.localScale = Vector3.one;


			#region
			pointsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/BasesPoints/BasePointAfros"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			pointsCurrent.name = "Point Currents";			
			// Lo agregamos como hijo del HUD
			pointsCurrent.transform.parent = this.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			pointsCurrent.transform.localPosition = new Vector3(0, -482, 0);
			pointsCurrent.transform.localRotation = Quaternion.identity;
			pointsCurrent.transform.localScale = Vector3.one;
			#endregion
			

			indiceBotonJugarMas = 7;
			break;

		case "Guambianos":


			//Caras
			itemsOfGrid [0] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Guambiano_1"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [0].name = "Avatar Guambiano 1";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [0].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [0].transform.localPosition = Vector3.zero;
			itemsOfGrid [0].transform.localRotation = Quaternion.identity;
			itemsOfGrid [0].transform.localScale = Vector3.one;
			
			//Caras
			itemsOfGrid [1] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Guambiano_2"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [1].name = "Avatar Guambiano 2";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [1].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [1].transform.localPosition = Vector3.zero;
			itemsOfGrid [1].transform.localRotation = Quaternion.identity;
			itemsOfGrid [1].transform.localScale = Vector3.one;

			itemsOfGrid [2] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Guambiano_3"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [2].name = "Avatar Guambiano 3";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [2].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [2].transform.localPosition = Vector3.zero;
			itemsOfGrid [2].transform.localRotation = Quaternion.identity;
			itemsOfGrid [2].transform.localScale = Vector3.one;



			#region
			pointsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/BasesPoints/BasePointGuambianos"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			pointsCurrent.name = "Point Currents";			
			// Lo agregamos como hijo del HUD
			pointsCurrent.transform.parent = this.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			pointsCurrent.transform.localPosition = new Vector3(0, -482, 0);
			pointsCurrent.transform.localRotation = Quaternion.identity;
			pointsCurrent.transform.localScale = Vector3.one;
			#endregion

			indiceBotonJugarMas = 2;
			
			
			break;

		case "Nasa":


			//Caras
			itemsOfGrid [0] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Nasa_1"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [0].name = "Avatar Nasa 1";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [0].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [0].transform.localPosition = Vector3.zero;
			itemsOfGrid [0].transform.localRotation = Quaternion.identity;
			itemsOfGrid [0].transform.localScale = Vector3.one;
			
			//Caras
			itemsOfGrid [1] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Nasa_2"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [1].name = "Avatar Nasa 2";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [1].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [1].transform.localPosition = Vector3.zero;
			itemsOfGrid [1].transform.localRotation = Quaternion.identity;
			itemsOfGrid [1].transform.localScale = Vector3.one;

			itemsOfGrid [2] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Nasa_3"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [2].name = "Avatar Nasa 3";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [2].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [2].transform.localPosition = Vector3.zero;
			itemsOfGrid [2].transform.localRotation = Quaternion.identity;
			itemsOfGrid [2].transform.localScale = Vector3.one;

			itemsOfGrid [3] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Nasa_4"));			
			// Dado que ity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [3].name = "Avatar Nasa 4";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [3].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [3].transform.localPosition = Vector3.zero;
			itemsOfGrid [3].transform.localRotation = Quaternion.identity;
			itemsOfGrid [3].transform.localScale = Vector3.one;

			itemsOfGrid [4] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Nasa_5"));			
			// Dado que ity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [4].name = "Avatar Nasa 5";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [4].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [4].transform.localPosition = Vector3.zero;
			itemsOfGrid [4].transform.localRotation = Quaternion.identity;
			itemsOfGrid [4].transform.localScale = Vector3.one;

			itemsOfGrid [5] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Nasa_6"));			
			// Dado que ity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [5].name = "Avatar Nasa 6";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [5].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [5].transform.localPosition = Vector3.zero;
			itemsOfGrid [5].transform.localRotation = Quaternion.identity;
			itemsOfGrid [5].transform.localScale = Vector3.one;

			itemsOfGrid [6] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Nasa_7"));			
			// Dado que ity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [6].name = "Avatar Nasa 7";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [6].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [6].transform.localPosition = Vector3.zero;
			itemsOfGrid [6].transform.localRotation = Quaternion.identity;
			itemsOfGrid [6].transform.localScale = Vector3.one;

			itemsOfGrid [7] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Nasa_8"));			
			// Dado que ity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [7].name = "Avatar Nasa 8";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [7].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [7].transform.localPosition = Vector3.zero;
			itemsOfGrid [7].transform.localRotation = Quaternion.identity;
			itemsOfGrid [7].transform.localScale = Vector3.one;

			#region
			pointsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/BasesPoints/BasePointNasa"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			pointsCurrent.name = "Point Currents";			
			// Lo agregamos como hijo del HUD
			pointsCurrent.transform.parent = this.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			pointsCurrent.transform.localPosition = new Vector3(0, -482, 0);
			pointsCurrent.transform.localRotation = Quaternion.identity;
			pointsCurrent.transform.localScale = Vector3.one;
			#endregion

			indiceBotonJugarMas = 7;
			
			
			break;

		case "Yanaconas":


			//Caras
			itemsOfGrid [0] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Yanaconas_1"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [0].name = "Avatar Yanaconas 1";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [0].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [0].transform.localPosition = Vector3.zero;
			itemsOfGrid [0].transform.localRotation = Quaternion.identity;
			itemsOfGrid [0].transform.localScale = Vector3.one;
			
			//Caras
			itemsOfGrid [1] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Yanaconas_2"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [1].name = "Avatar Yanaconas 2";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [1].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [1].transform.localPosition = Vector3.zero;
			itemsOfGrid [1].transform.localRotation = Quaternion.identity;
			itemsOfGrid [1].transform.localScale = Vector3.one;

			//Caras
			itemsOfGrid [2] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Yanaconas_3"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [2].name = "Avatar Yanaconas 3";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [2].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [2].transform.localPosition = Vector3.zero;
			itemsOfGrid [2].transform.localRotation = Quaternion.identity;
			itemsOfGrid [2].transform.localScale = Vector3.one;

			//Caras
			itemsOfGrid [3] = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Avatars/Avatar_Yanaconas_4"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			itemsOfGrid [3].name = "Avatar Yanaconas 4";			
			// Lo agregamos como hijo del HUD
			itemsOfGrid [3].transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			itemsOfGrid [3].transform.localPosition = Vector3.zero;
			itemsOfGrid [3].transform.localRotation = Quaternion.identity;
			itemsOfGrid [3].transform.localScale = Vector3.one;

			#region
			pointsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/BasesPoints/BasePointYanaconas"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			pointsCurrent.name = "Point Currents";			
			// Lo agregamos como hijo del HUD
			pointsCurrent.transform.parent = this.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			pointsCurrent.transform.localPosition = new Vector3(0, -482, 0);
			pointsCurrent.transform.localRotation = Quaternion.identity;
			pointsCurrent.transform.localScale = Vector3.one;
			#endregion

			indiceBotonJugarMas = 3;
			
			
			break;
			default:
				break;
			}

		foreach (Transform item in pointsCurrent.transform)
		{

			points.Add(item.gameObject);

		}


//				foreach (Transform item in grid.transform) {
//						itemsOfGrid.Add (item.gameObject);
//				}
	
				NGUITools.SetActive (arrows [1], false);
				grid.onFinished += MetodoOnDrag;
	
		}

		public void SelectItemsRegion ()
		{

				
		}

	void AdvanceByButtonsRight()
	{
		Debug.Log("Se presiono boton derecho");


		for (int n = 0; n < itemsOfGrid.Count; n++)
		{
			if(objCentered.Equals(itemsOfGrid[n]) && !objCentered.Equals(itemsOfGrid[itemsOfGrid.Count - 1]))
			{
				Debug.Log("<------------------------------------------------------ " + itemsOfGrid[n + 1].name);
				itemsOfGrid[n + 1].SendMessage("OnClick");
				
			}
		}
	}

	void AdvanceByButtonsLeft()
	{
		Debug.Log("Se presiono boton izquierdo");
		for (int n = 0; n < itemsOfGrid.Count; n++)
		{
			if(objCentered.Equals(itemsOfGrid[n]) && !objCentered.Equals(itemsOfGrid[0]))
			{
				Debug.Log("<------------------------------------------------------ " + itemsOfGrid[n - 1].name);
				itemsOfGrid[n - 1].SendMessage("OnClick");
				
			}
		}
	}



		void MetodoOnDrag ()
		{
//				Debug.LogWarning ("termino el drag");
				objCentered = grid.centeredObject;



		for (int d = 0; d < itemsOfGrid.Count; d++)
		{
			if(objCentered.Equals(itemsOfGrid[d]))
			{
				points[d].SendMessage("OnClick");
				
			}
		}


		if (objCentered.Equals (itemsOfGrid[indiceBotonJugarMas]))
		{
			buttonPlayMore.transform.parent.GetComponent<TweenPosition>().PlayForward();
		}


		
//				if (objCentered.Equals (itemsOfGrid [0])) {
//						NGUITools.SetActive (arrows [0], true);
//						NGUITools.SetActive (arrows [1], false);
//				} else if (objCentered.Equals (itemsOfGrid [itemsOfGrid.Count - 1])) {
//						NGUITools.SetActive (arrows [0], false);
//						NGUITools.SetActive (arrows [1], true);
//				} 

		if(objCentered.Equals(itemsOfGrid[0]))
		{
			NGUITools.SetActive(arrows[0], true);
			NGUITools.SetActive(arrows[1], false);
		}
		else if(objCentered.Equals(itemsOfGrid[indiceBotonJugarMas]))
		{
			NGUITools.SetActive(arrows[0], false);
			NGUITools.SetActive(arrows[1], true);
		}
		else
		{
			NGUITools.SetActive(arrows[0], true);
			NGUITools.SetActive(arrows[1], true);
		}
		}
}
