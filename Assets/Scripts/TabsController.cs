﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TabsController : MonoBehaviour {

	public List <GameObject> listaItems;
	public List <GameObject> listaTabs;
	public UISprite [] listaPartes;
	public List <GameObject> listaColores;

	public UISprite cuerpo;
	public string [] nombres;
	public string [] nombresObtenidos;
	public static string devolver;
	public UIButton botonConfirmar;

	// Use this for initialization
	void Start ()
	{

		EventDelegate.Add(botonConfirmar.onClick, delegate() { 
			nombres = new string[5];		
		
			for (int r = 0; r < nombres.Length; r++)
			{
				nombres[r] = listaPartes[r].spriteName;
			}
			
			PlayerPrefsX.SetStringArray("NombresPartes", nombres);	});

		foreach (GameObject currentTab in listaTabs)
		{
			UIEventListener.Get(currentTab).onClick += Metodo;

		}

		foreach (GameObject currentItem in listaItems)
		{
			UIEventListener.Get(currentItem).onClick += Customizar;
			
		}

		foreach (GameObject currentColor in listaColores)
		{
			UIEventListener.Get(currentColor).onClick += ChangeColor;
		}

	
	}

	public static string [] ObtenerCaracteristicas(string [] arregloAObtener)
	{
		arregloAObtener = PlayerPrefsX.GetStringArray("NombresPartes");
		return arregloAObtener;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.F))
		{ 


		}
	
	}

	public void ChangeColor(GameObject gO)
	{
		cuerpo.color = gO.transform.GetChild(0).GetComponent<UISprite>().color;
		listaPartes[1].color = gO.transform.GetChild(0).GetComponent<UISprite>().color;
	}
	public void Customizar(GameObject gO)
	{
		string nameComplete;
		string [] partsOfName;
		
		nameComplete = gO.name;
		partsOfName = nameComplete.Split(new char[]{ '_' });

		switch (partsOfName[0])
		{
		case "Cabello":
				listaPartes[0].spriteName = "Cabello_" + partsOfName[1];
					break;

		case "Cabeza":
				listaPartes[1].spriteName = "Cabeza_" + partsOfName[1];
			break;

		case "Camiseta":
				listaPartes[2].spriteName = "Camiseta_" + partsOfName[1];
			listaPartes[2].MakePixelPerfect();
			break;

		case "Pantalones":
				listaPartes[3].spriteName = "Pantalones_" + partsOfName[1];
			break;

		case "Zapatos":
				listaPartes[4].spriteName = "Zapatos_" + partsOfName[1];
			break;
				default:
						break;
		}




	}
	public void Metodo(GameObject gO)
	{


		switch (gO.name)
		{
		case "Tab1":
			foreach (GameObject currentItem in listaItems)
			{
				string nameComplete;
				string [] partsOfName;

				nameComplete = currentItem.name;
				partsOfName = nameComplete.Split(new char[]{ '_' });


				currentItem.name = "Cabello_" + partsOfName[1];
				currentItem.transform.GetChild(1).GetComponent<UISprite>().spriteName = currentItem.name;
				currentItem.transform.GetChild(1).GetComponent<UISprite>().MakePixelPerfect();

			}
			break;
		case "Tab2":
			foreach (GameObject currentItem in listaItems)
			{
				string nameComplete;
				string [] partsOfName;

				nameComplete = currentItem.name;
				partsOfName = nameComplete.Split(new char[]{ '_' });
				

				currentItem.name = "Cabeza_" + partsOfName[1];
				currentItem.transform.GetChild(1).GetComponent<UISprite>().spriteName = currentItem.name;
				currentItem.transform.GetChild(1).GetComponent<UISprite>().MakePixelPerfect();
			}
			break;
		case "Tab3":

			foreach (GameObject currentItem in listaItems)
			{
				string nameComplete;
				string [] partsOfName;

				nameComplete = currentItem.name;
				partsOfName = nameComplete.Split(new char[]{ '_' });
				

				currentItem.name = "Camiseta_" + partsOfName[1];
				currentItem.transform.GetChild(1).GetComponent<UISprite>().spriteName = currentItem.name;
				currentItem.transform.GetChild(1).GetComponent<UISprite>().MakePixelPerfect();
				
			}
			break;
		case "Tab4":
			foreach (GameObject currentItem in listaItems)
			{
				string nameComplete;
				string [] partsOfName;

				nameComplete = currentItem.name;
				partsOfName = nameComplete.Split(new char[]{ '_' });
				

				currentItem.name = "Pantalones_" + partsOfName[1];
				currentItem.transform.GetChild(1).GetComponent<UISprite>().spriteName = currentItem.name;
				currentItem.transform.GetChild(1).GetComponent<UISprite>().MakePixelPerfect();
				
			}
			break;
		case "Tab5":
			foreach (GameObject currentItem in listaItems)
			{
				string nameComplete;
				string [] partsOfName;

				nameComplete = currentItem.name;
				partsOfName = nameComplete.Split(new char[]{ '_' });
				

				currentItem.name = "Zapatos_" + partsOfName[1];
				currentItem.transform.GetChild(1).GetComponent<UISprite>().spriteName = currentItem.name;
				currentItem.transform.GetChild(1).GetComponent<UISprite>().MakePixelPerfect();
				
			}
			break;
		case "Tab6":
			break;
		case "Tab7":
			break;
				default:
						break;
		}

	}
}
