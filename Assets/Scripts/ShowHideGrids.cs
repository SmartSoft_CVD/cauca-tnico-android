﻿using UnityEngine;
using System.Collections;

public class ShowHideGrids : MonoBehaviour {

	public GameObject activate;
	public GameObject [] desactivate;

	// Use this for initialization
	void Start ()
	{
		desactivate = new GameObject[2];
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static void ShowCurrentScrollView(GameObject partAvatar, GameObject active, GameObject desactivate1, GameObject desactivate2)
	{
		EventDelegate.Add(partAvatar.GetComponent<UIButton>().onClick, delegate()
		                  { partAvatar.GetComponent<ShowHideGrids>().activate = active;
			partAvatar.GetComponent<ShowHideGrids>().desactivate[0] = desactivate1;
			partAvatar.GetComponent<ShowHideGrids>().desactivate[1] = desactivate2;
			
			NGUITools.SetActive(active.transform.parent.gameObject, true);
			NGUITools.SetActive(desactivate1.transform.parent.gameObject, false);
			NGUITools.SetActive(desactivate2.transform.parent.gameObject, false);
		});
	}

	public void Function()
	{
		NGUITools.SetActive(activate.transform.parent.gameObject, true);
		NGUITools.SetActive(desactivate[0].transform.parent.gameObject, false);
		NGUITools.SetActive(desactivate[1].transform.parent.gameObject, false);
	}
}
