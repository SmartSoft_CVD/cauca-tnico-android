﻿using UnityEngine;
using System.Collections;

public class NavigationMap : MonoBehaviour {

	private int numeroEscena;
	void Awake()
	{
		FB.Init (OnInitComplete, OnHideUnity);
	}
	// Use this for initialization
	void Start ()
	{
		Time.timeScale = 1.0f;
		StartCoroutine(StartGame());
	//	PlayerPrefsX.SetBool("Confetti", false);

	}

	IEnumerator StartGame(){
		yield return new WaitForEndOfFrame();


		Hud.switchToState (eScreen.LOGO);
//		if(GoToMenu.startGame)
//		{
//			Hud.switchToState (eScreen.MINIJUEGO_INICIO);
//		}else
//		{ 
//			Hud.switchToState (eScreen.LOGO);
//		}

		PlayerPrefs.SetString("Escena", Application.loadedLevelName);

		if(PlayerPrefs.GetString("Escena") == "TestScene")
		{
			numeroEscena = PlayerPrefs.GetInt("Numero", 0);
			numeroEscena++;
			PlayerPrefs.SetInt("Numero", numeroEscena);
		}


		if(PlayerPrefs.GetInt("Numero") == 1)
		{
			PlayerPrefsX.SetBool("Inicio", true);
		}


		yield return new WaitForEndOfFrame();


//		if(PlayerPrefsX.GetBool("Inicio") == true){
//			Hud.switchToState (eScreen.LOGO);}

//		if(PlayerPrefsX.GetBool("Inicio") == false)
//		{
//			Hud.switchToState (eScreen.MINIJUEGO_INICIO);
//
//			
//		}

		Debug.Log(" <------------------------------------------------------ Numero de veces en que la escena: " + PlayerPrefs.GetString("Escena") + " ha cargado: " + PlayerPrefs.GetInt("Numero") + " ---------------------------------------------> ");
//		if(PlayerPrefs.GetInt("StartGame") == 0)
//		{
//			Hud.switchToState (eScreen.LOGO);
//
//		}
//		if(PlayerPrefs.GetInt("StartGame") == 1)
//		{
//			Debug.LogWarning("Entro al principio");
//
//			Hud.switchToState (eScreen.LOGO);
//		}
//
//		if(PlayerPrefs.GetInt("StartGame") == 2)
//		{
//			Debug.Log("Entro al final");
//
//			Hud.switchToState (eScreen.MINIJUEGO_INICIO);
//		}
	}

	public void Update()
	{
		//Debug.LogError("Este es el valor de la llave: "  + PlayerPrefsX.GetBool("Inicio"));
	}
	private void OnInitComplete ()
	{
		Debug.Log ("FB.Init completed: Is user logged in? " + FB.IsLoggedIn);
		if(this.gameObject != null)
			enabled = true;
	}

	//Called when the SDk tries to display HTML content
	private void OnHideUnity(bool isGameShown){
		if(!isGameShown){
			Time.timeScale = 0;
		} else{
			Time.timeScale = 1;
		}
	}


	
	/// <summary>
	/// Cambia la interfaz por la de la siguiente pantalla
	/// </summary>
	/// <param name="gO"> botón que permite hacer la transición a la siguiente pantalla </param>
	/// <param name="screenToLoad"> La pantalla que deseamos cargar.</param>
	public static void ChangeOfScreen(GameObject gO, eScreen screenToLoad)
	{
		if(gO.GetComponent<UIButton>() != null)
		{
		

			UIButton compButton = gO.GetComponent<UIButton>();
			EventDelegate.Add(compButton.onClick, delegate()
	          {
					UIPlayTween playTweens = gO.GetComponent<UIPlayTween>();
					

					if(playTweens == null){
					//						Debug.LogError("El objeto "  + gO.name + " no contiene un componente UIPlayTween");
					}

					if(compButton.gameObject.name != "ButtonReiniciar")
					{
						playTweens.tweenTarget = GameObject.Find(Hud.rootHUD);
						EventDelegate.Add(playTweens.onFinished, delegate() { Hud.switchToState(screenToLoad); });
					}
					else
					{
						
						Hud.switchToState(eScreen.MINIJUEGO_INICIO);
					}

			   });
		}
		
	}



}
