{"frames": {

"Accesorios_Campesino_1.png":
{
	"frame": {"x":2,"y":2,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Accesorios_Campesino_2.png":
{
	"frame": {"x":2,"y":348,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Accesorios_Guambiano_2.png":
{
	"frame": {"x":2,"y":694,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Accesorios_Guambiano_3.png":
{
	"frame": {"x":2,"y":1040,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Accesorios_Guambiano_4.png":
{
	"frame": {"x":2,"y":1386,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Accesorios_Guambiano_5.png":
{
	"frame": {"x":229,"y":2,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Accesorios_Nasa_2.png":
{
	"frame": {"x":229,"y":348,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Accesorios_Nasa_3.png":
{
	"frame": {"x":229,"y":694,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Accesorios_Nasa_4.png":
{
	"frame": {"x":229,"y":1040,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Accesorios_Yanaconas_1.png":
{
	"frame": {"x":229,"y":1386,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Afro_1.png":
{
	"frame": {"x":456,"y":2,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Afro_2.png":
{
	"frame": {"x":683,"y":2,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Afro_3.png":
{
	"frame": {"x":910,"y":2,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Afro_4.png":
{
	"frame": {"x":1137,"y":2,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Campesino_1.png":
{
	"frame": {"x":1364,"y":2,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Campesino_2.png":
{
	"frame": {"x":1591,"y":2,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Campesino_3.png":
{
	"frame": {"x":456,"y":348,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Campesino_4.png":
{
	"frame": {"x":456,"y":694,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Campesino_5.png":
{
	"frame": {"x":456,"y":1040,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Guambiano_1.png":
{
	"frame": {"x":456,"y":1386,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Guambiano_2.png":
{
	"frame": {"x":683,"y":348,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Guambiano_3.png":
{
	"frame": {"x":910,"y":348,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Nasa_1.png":
{
	"frame": {"x":1137,"y":348,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Nasa_2.png":
{
	"frame": {"x":1364,"y":348,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Nasa_3.png":
{
	"frame": {"x":1591,"y":348,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Nasa_4.png":
{
	"frame": {"x":683,"y":694,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Nasa_5.png":
{
	"frame": {"x":683,"y":1040,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Nasa_6.png":
{
	"frame": {"x":683,"y":1386,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Nasa_7.png":
{
	"frame": {"x":910,"y":694,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Yanaconas_1.png":
{
	"frame": {"x":910,"y":1040,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Yanaconas_2.png":
{
	"frame": {"x":910,"y":1386,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Pantalon_Yanaconas_3.png":
{
	"frame": {"x":1137,"y":694,"w":225,"h":344},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":344},
	"sourceSize": {"w":225,"h":344}
},
"Sombreros_Afro_1.png":
{
	"frame": {"x":2,"y":1732,"w":225,"h":203},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":203},
	"sourceSize": {"w":225,"h":203}
},
"Sombreros_Afro_2.png":
{
	"frame": {"x":229,"y":1732,"w":225,"h":203},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":203},
	"sourceSize": {"w":225,"h":203}
},
"Sombreros_Afro_4.png":
{
	"frame": {"x":456,"y":1732,"w":225,"h":203},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":203},
	"sourceSize": {"w":225,"h":203}
},
"Sombreros_Afro_5.png":
{
	"frame": {"x":683,"y":1732,"w":225,"h":203},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":203},
	"sourceSize": {"w":225,"h":203}
},
"Sombreros_Afro_8.png":
{
	"frame": {"x":910,"y":1732,"w":225,"h":203},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":203},
	"sourceSize": {"w":225,"h":203}
},
"Sombreros_Guambiano_1.png":
{
	"frame": {"x":1364,"y":694,"w":225,"h":203},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":203},
	"sourceSize": {"w":225,"h":203}
},
"Sombreros_Guambiano_7.png":
{
	"frame": {"x":1591,"y":694,"w":225,"h":203},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":203},
	"sourceSize": {"w":225,"h":203}
},
"Sombreros_Yanaconas_2.png":
{
	"frame": {"x":1364,"y":899,"w":225,"h":203},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":203},
	"sourceSize": {"w":225,"h":203}
},
"Sombreros_afro_3.png":
{
	"frame": {"x":1137,"y":1040,"w":225,"h":203},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":203},
	"sourceSize": {"w":225,"h":203}
},
"Sombreros_naza_2.png":
{
	"frame": {"x":1591,"y":899,"w":225,"h":203},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":203},
	"sourceSize": {"w":225,"h":203}
},
"Zapatos_Afro_1.png":
{
	"frame": {"x":1364,"y":1104,"w":225,"h":148},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":148},
	"sourceSize": {"w":225,"h":148}
},
"Zapatos_Afro_2.png":
{
	"frame": {"x":1137,"y":1245,"w":225,"h":148},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":148},
	"sourceSize": {"w":225,"h":148}
},
"Zapatos_Campesino_1.png":
{
	"frame": {"x":1591,"y":1104,"w":225,"h":148},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":148},
	"sourceSize": {"w":225,"h":148}
},
"Zapatos_Campesino_2.png":
{
	"frame": {"x":1364,"y":1254,"w":225,"h":148},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":148},
	"sourceSize": {"w":225,"h":148}
},
"Zapatos_Campesino_3.png":
{
	"frame": {"x":1137,"y":1395,"w":225,"h":148},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":148},
	"sourceSize": {"w":225,"h":148}
},
"Zapatos_Guambiano_1.png":
{
	"frame": {"x":1591,"y":1254,"w":225,"h":148},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":148},
	"sourceSize": {"w":225,"h":148}
},
"Zapatos_Guambiano_2.png":
{
	"frame": {"x":1137,"y":1545,"w":225,"h":148},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":148},
	"sourceSize": {"w":225,"h":148}
},
"Zapatos_Nasa_1.png":
{
	"frame": {"x":1137,"y":1695,"w":225,"h":148},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":148},
	"sourceSize": {"w":225,"h":148}
},
"Zapatos_Nasa_2.png":
{
	"frame": {"x":1364,"y":1404,"w":225,"h":148},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":148},
	"sourceSize": {"w":225,"h":148}
},
"Zapatos_Nasa_3.png":
{
	"frame": {"x":1591,"y":1404,"w":225,"h":148},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":148},
	"sourceSize": {"w":225,"h":148}
},
"Zapatos_Nasa_4.png":
{
	"frame": {"x":1364,"y":1554,"w":225,"h":148},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":148},
	"sourceSize": {"w":225,"h":148}
},
"Zapatos_Nasa_5.png":
{
	"frame": {"x":1364,"y":1704,"w":225,"h":148},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":148},
	"sourceSize": {"w":225,"h":148}
},
"Zapatos_Yanaconas_1.png":
{
	"frame": {"x":1591,"y":1554,"w":225,"h":148},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":148},
	"sourceSize": {"w":225,"h":148}
},
"Zapatos_Yanaconas_2.png":
{
	"frame": {"x":1591,"y":1704,"w":225,"h":148},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":225,"h":148},
	"sourceSize": {"w":225,"h":148}
}},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker",
	"version": "1.0",
	"image": "AtlasAparienciaSegundo2x.png",
	"format": "RGBA8888",
	"size": {"w":1937,"h":1937},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:7f7a8e56d3078da1bf25c56adae997d3:f0a983e4976fdedaaf807535b8780e0c:9f26ca64b9ea81498330acfc2555c2b1$"
}
}
